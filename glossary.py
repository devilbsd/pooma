menu_default = {}
menu_default['A'] = {"title":"PooMA","url":"index"}
menu_default['B'] = {"title":"Repo","url":"repo"}
menu_default['C'] = {"title":"Agents","url":"agents"}
menu_default['D'] = {"title":"Policy","url":"policy"}
menu_default['E'] = {"title":"Tools","url":"oneclickcli"}
menu_default['F'] = {"title":"LogIn","url":"login"}
menu_default['G'] = {"title":"About","url":"about"}

menu_repo = {}
menu_repo['A'] = {"title":"PooMA","url":"index"}
menu_repo['B'] = {"title":"Repo","url":"repo"}
menu_repo['C'] = {"title":"Tags","url":"tags"}
menu_repo['D'] = {"title":"","url":""}
menu_repo['E'] = {"title":"","url":""}
menu_repo['F'] = {"title":"","url":""}
menu_repo['G'] = {"title":"","url":""}

menu_tools = {}
menu_tools['A'] = {"title":"PooMA","url":"index"}
menu_tools['B'] = {"title":"Tools","url":"oneclickcli"}
menu_tools['C'] = {"title":"1ClkCli","url":"oneclickcli"}
menu_tools['D'] = {"title":"Phishing","url":"phishing"}
menu_tools['E'] = {"title":"","url":""}
menu_tools['F'] = {"title":"","url":""}
menu_tools['G'] = {"title":"","url":""}

menu_phishing = {}
menu_phishing ['A'] = {"title":"PooMA","url":"index"}
menu_phishing ['B'] = {"title":"Phishing","url":"phishing"}
menu_phishing ['C'] = {"title":"Templates","url":"phishingtemplates"}
menu_phishing ['D'] = {"title":"Targets","url":"phishingtargets"}
menu_phishing ['E'] = {"title":"","url":""}
menu_phishing ['F'] = {"title":"","url":""}
menu_phishing ['G'] = {"title":"","url":""}

menu_settings = {}
menu_settings['A'] = {"title":"PooMA","url":"index"}
menu_settings['B'] = {"title":"Policy","url":"policy"}
menu_settings['C'] = {"title":"Feeds","url":"feeds"}
menu_settings['D'] = {"title":"SaaS","url": "saas"}
menu_settings['E'] = {"title":"Patterns","url":"patterns"}
menu_settings['F'] = {"title":"Storage","url":"database"}
menu_settings['G'] = {"title":"","url": ""}

menu_agents = {}
menu_agents['A'] = {"title":"PooMA","url":"index"}
menu_agents['B'] = {"title":"Agents","url": "agents"}
menu_agents['D'] = {"title":"Crawlers","url":"crawlers"}
menu_agents['C'] = {"title":"Scripts","url":"scripts"}
menu_agents['E'] = {"title":"","url":""}
menu_agents['F'] = {"title":"","url":""}
menu_agents['G'] = {"title":"","url": ""}

os_images={}
os_images['e50e99f3-5963-4573-af9e-e3f4750b55e2'] = '''WinXP SP3 Off 2k3&2k7'''
os_images['7e6fe36e-889e-4c25-8704-56378f0830df'] = '''Win7 32b Off 2k3&2k7'''
os_images['8d188031-1010-4466-828b-0cd13d4303ff'] = '''Win7 32b Off 2k10'''
os_images['5e5de275-a103-4f67-b55b-47532918fa59'] = '''Win7 32b,Office 2013,Adobe 11'''
os_images['3ff3ddae-e7fd-4969-818c-d5f1a2be336d'] = '''Win7 64b,Office 2010,Adobe 11'''
os_images['6c453c9b-20f7-471a-956c-3198a868dc92'] = '''Win8.1 64b Off 2k1364b'''
os_images['10b4a9c6-e414-425c-ae8b-fe4dd7b25244'] = '''Windows 10 64b Off 2k16 AdobeDC'''

EMULATION_ENV = []

WINXP={}
WINXP['id'] ='e50e99f3-5963-4573-af9e-e3f4750b55e2'
WINXP['arch'] = 32
WINXP['os'] ='Windows XP Service Pack 3'
WINXP['apps'] = '''Office 2003/7, Adobe 9'''
WINXP['rev'] = 204

WIN732A={}
WIN732A['id'] ='7e6fe36e-889e-4c25-8704-56378f0830df'
WIN732A['arch'] = 32
WIN732A['os'] ='Windows 7'
WIN732A['apps'] = '''Office 2003/7, Adobe 9'''
WIN732A['rev'] = 204

WIN764={}
WIN764['id'] ='3ff3ddae-e7fd-4969-818c-d5f1a2be336d'
WIN764['arch'] = 64
WIN764['os'] ='Windows 7'
WIN764['apps'] = '''Office 2010,Adobe 11'''
WIN764['rev'] = 204

WIN81={}
WIN81['id'] ='6c453c9b-20f7-471a-956c-3198a868dc92'
WIN81['arch'] = 64
WIN81['os'] ='Windows 8.1'
WIN81['apps'] = '''Office 2013, Adobe 11'''
WIN81['rev'] = 204

WIN10={}
WIN10['id'] ='10b4a9c6-e414-425c-ae8b-fe4dd7b25244'
WIN10['arch'] = 64
WIN10['os'] ='Windows 10'
WIN10['apps'] = '''Office 2016,Adobe DC'''
WIN10['rev'] = 204

EMULATION_ENV.append(WINXP)
EMULATION_ENV.append(WIN732A)
EMULATION_ENV.append(WIN764)
EMULATION_ENV.append(WIN81)
EMULATION_ENV.append(WIN10)

defaultFilter = {}
defaultFilter['filtername'] = 'DefaultFilter'
defaultFilter['logic'] = 'and'
defaultFilter['tags'] = []
defaultFilter['content'] = []
defaultFilter['verdict'] = []
defaultFilter['source'] = []
defaultFilter['status'] = []
defaultFilter['section'] = 'widgetFilter'
defaultFilter['active'] = True
defaultFilter['edit'] = 'false'
defaultFilter['maxItems'] = 20
          



md5list_tmpl = '''#for $md5 in $md5set
$md5\r
#end for'''

#Status for a file

FOUND = 1001
UPLOAD_SUCCES = 1002
PENDING = 1003
NOT_FOUND = 1004


VERD_MALICIOUS = {"$or":[{'verdict':'matched'},{'verdict':'malicious'}]}

VERD_SAFE = {'verdict':'benign'}
STATUS_EMULATING = {'status':'emulating'}
WITH_FORENSICS = { 'forensics': { '$exists': True } }
BY_SIGNATURE = "sandblast['av']['malware_info']['signature_name']"
CLOUD_FEEDS = ['cpsb_cloud']


ALLBLADES = ['av','te','extraction']
TETEXBLADES = ['te','extraction']
AVTEBLADES = ['av','te']
TEBLADE = ['te']


def UPLOAD_MALWARE_REQUEST(md5=None, filename=None):
    request = {"request": {"md5": md5,
                           "file_name": filename,
                          #"features": ["te"], .. by default
                          #Selecting no images means emulate with the default se
                           "te": {"reports": ["pdf","xml"]},
                                                 "images": [{"id": "7e6fe36e-889e-4c25-8704-56378f0830df","revision": 1}, #Win7 32bit
                                                            {"id": "e50e99f3-5963-4573-af9e-e3f4750b55e2","revision": 1}, #WinXP
                                                            {"id": "8d188031-1010-4466-828b-0cd13d4303ff","revision": 1},
                                                            {"id": "5e5de275-a103-4f67-b55b-47532918fa59","revision": 1},
                                                            {"id": "3ff3ddae-e7fd-4969-818c-d5f1a2be336d","revision": 1},
                                                            {"id": "6c453c9b-20f7-471a-956c-3198a868dc92","revision": 1}] 
                                                    }}
    return request

def EXTRACTION_REQUEST(md5=None, filename=None):
    request = {"request": {"md5": md5,
                     "file_name": filename,
                      "features": ["extraction"],
                      "extraction": {"method": "convert"}}}
    return request



def QUERY_MALWARE_REQUEST(md5=None):
    request = {"request": [{"md5": md5,
                            "features": ["av","te"],
                            "te": {"reports": ["pdf"]}
                            }]
               }
    return request


def QUERY_REQUEST_AV(md5=None, sha1=None):
    request = {"request": [{"md5": md5,
                            "sha1":sha1,
                            "features": ["av","te"],
                            "te": {"reports": ["pdf"]}
                            }]
               }
    return request
