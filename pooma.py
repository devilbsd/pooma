import config
import messages
import messages as msg
import mimetypes
import te_supported_mimetypes as TE_SUPPORTED
import tex_supported_mimetypes as TEX_SUPPORTED
import requests
import glossary
import json
from pymongo import MongoClient
import gridfs
import logging
from log4mongo.handlers import MongoHandler
import threading
import time
import tinys3
import uuid
import base64

client = MongoClient(config.mongodburi)
db = client.pooma
repofs = gridfs.GridFS(db)

## MongoDB hosted @ Atlas
atlasClient = MongoClient(config.atlasdburi)
reputationdb = client.ReputationDB

logging.basicConfig(filename='pooma_module.log', filemode='w', level=logging.DEBUG)

mongoLogger = logging.getLogger('PooMALog')
mongoHandler = MongoHandler(host=config.mongodburi,
                            database_name='pooma',
                            collection='logs')
mongoLogger.addHandler(mongoHandler)

mimetypes.init()

def loadSettings():
        return db.settings.find_one({"section": "sandblast"})


def updateFileMetadata(status = None, verdict = None, fileID=None, newMetadata = None, sandblastReputation = None,
                       sandblastEmulation = None, sandblastExtraction=None):
        '''newMetadata fileMetadata as the info of the file and the parameter or parameters that
        will be updated'''
      
        if newMetadata == None:
                newMetadata = {}
        newMetadata['status'] = status       
        
        if verdict != None:        
                newMetadata['verdict'] = verdict
                
        if sandblastReputation != None:        
                newMetadata['sandblast_reputation'] = sandblastReputation
                
        if sandblastEmulation != None:        
                newMetadata['sandblast_emulation'] = sandblastEmulation
                
        if sandblastExtraction != None:
                newMetadata['sandblast_extraction'] = sandblastExtraction
                
        db.fs.files.update_one({'_id': fileID}, {'$set': newMetadata })


def urlcrawler(customer_file=None):
        '''md5 is of the file we are going to crawl'''
         #Get info from file
        print customer_file.file
        print dir(customer_file.file)
        for line in customer_file.file.readlines(): 
            #print line
            pooma_url = "http://172.20.22.66/ipurllookup/?ipurl="+ line
            print pooma_url
            fetchResp = requests.post(pooma_url)
            print "##### Query Response L#####"
            print fetchResp.text
        return 0

def phishtank(url = None, url_id=None):
        ##Starting "FromURLFetching & Emulation"
        pooma_url = "http://172.20.22.66/fetchfile/?url=" + url
        fetchResp = requests.post(pooma_url)
        ##Starts PhishTank reputatiom
        request = {'appkey': config.phishtank_key,
                   'format': 'json', 'url' : base64.b64encode(url) }
        response = requests.request("POST", config.phishtank_urlcheck, data=request)

        phishtank_response = json.loads(response.text)
        print phishtank_response
        print phishtank_response['results']['in_database']
        
        phishtank_results = {}
        phishtank_results['phishtank'] = phishtank_response
        phishtank_results['url_id'] = url_id
        phishtank_results['feed_vendor'] = 'phishtank'
        db.analysis.insert_one(phishtank_results)
        
        print url_id
        if phishtank_response['results']['in_database'] == True:
                print 'good'
                db.ipurl.update({'url_id': url_id}, {'$set':{'verdict': 'phishing'}})
        else:
                print 'bad'
                db.ipurl.update({'url_id': url_id}, {'$set':{'verdict': 'benign'}})
                        

def yaraanalysis(md5=None):
        #Get info from file
        fileMetadata = db.fs.files.find_one({"md5": md5})
        with repofs.get(fileMetadata['_id']) as filesample:
                print filesample
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'YARA'}
                s3_msg01 = "Uploading file to S3 BinaryAlert Bucket for Yara Analysis: " + filesample.filename 
                mongoLogger.info(s3_msg01 ,extra=logExtraMetadata)
                try:
                        conn = tinys3.Connection(config.s3_access_key, config.s3_private_key,
                                         default_bucket = config.s3_monitored_bucket, endpoint = config.s3_endpoint)
                        conn.upload(filesample.filename, filesample)
                except:
                        s3_msg01 = "Connection to AWS S3 bucket failed, try analysis again."
                        mongoLogger.info(s3_msg01 ,extra=logExtraMetadata)
                        
        
        
        
        
def send2analysis(md5=None):
        '''Analysis for sending a file to Sandblast should be made in 2 separate parallel threads.
        A. Malware Discovery thread: 3 features are used for this
               
        B. Extraction Thread
               
        '''
        #Check Support of files for each functionalitie
        fileMetadata = db.fs.files.find_one({"md5": md5})
        #This are the threads available for analysis, TE and TEX
        sandboxThread = threading.Thread(target=threatEmulation, args=(fileMetadata,))
        scrubbingThread = threading.Thread(target=threatExtraction, args=(fileMetadata,))
        
        if fileMetadata['content-type-mime'] in TE_SUPPORTED.MIMETYPELIST:
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TED'}
                mongoLogger.info(msg.TED_SUPPORTED(mimetype=fileMetadata['content-type-mime']),extra=logExtraMetadata)
                #Start Threat Emulation Thread
                sandboxThread.start()
        else:
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TED'}
                mongoLogger.info(msg.TED_NOT_SUPPORTED(mimetype=fileMetadata['content-type-mime']),extra=logExtraMetadata)
        
        
        #  Threat Extraction Threat
        if fileMetadata['content-type-mime'] in TEX_SUPPORTED.MIMETYPELIST:
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TEX'}
                mongoLogger.info(msg.TEX_SUPPORTED(mimetype=fileMetadata['content-type-mime']),extra=logExtraMetadata)
                #Start Threat Extraction Thread
                scrubbingThread.start()
        else:
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TEX'}
                mongoLogger.info(msg.TEX_NOT_SUPPORTED(mimetype=fileMetadata['content-type-mime']),extra=logExtraMetadata)
        
                
        return 0


def threatEmulation(fileMetadata=None):
        '''1. Query:  An 'av' & 'te' query is sent to the API.
                        if AV = FOUND set the verdict as 'maliciuous' 
                        if AV or TE = NOT FOUND found, UPLOAD the file
                        if TE = FOUND set the verdict
                                if verdict = malicious:
                                        fetch the reports
                                        mark 'malicious' if AV = NOT FOUND
                                if verdict = 'benign':
                                        mark as benign
                                        
                pre2. Check if file is supported for SandBlast Analysis      
                2. Query:  A 'te' & 'te_eb' UPLOAD query is sent to the API
                                if te or te_eb is FOUND:
                                        fetch sandbox forensics report
                                if te or te_eb is SUCCESFUL UPLOADED, wait for 1 minute and retry
                                        the same query without the file
                                if te or te_eb is PENDING, wait for 1 minute and retry the same
                                        query without the file
        '''
        maxTries = 20
        cookie = None
        avStatus = 0
        teStatus = 0
        teebStatus = 0
        fileStatus = 'unknown'
        fileVerdict = 'unknown'
        
        # 1st Phase - AntiVirus and TE Reputation Query
        #==============================================
        # - Only MD5 is sent to the API
        # - Not dependant of the type of the file since this is only a reputation query
        # - if found AV will respond with the info of the malware.
        # - if found TE will respond with the forensics report information and the verdicts
        #       even when we sent no file
        mongoLogger.info(msg.START_THREAD(tries=maxTries),extra={'sampleID': fileMetadata['md5']})
        mongoLogger.info(msg.START_1ST_PHASE,extra={'sampleID': fileMetadata['md5']})
        
        avRequest = buildRequest(querytype = 'av', fileMetadata=fileMetadata )
        avStatus, teStatus, texStatus, cookie, queryResult = execAPIRequest( request=avRequest, cookie=cookie, apiep='query', feature='av')
        
        if avStatus == glossary.FOUND:
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'RAV'}
                mongoLogger.info(msg.AV_MALICIOUS,extra=logExtraMetadata)
                # Status Unknow means sandblast knows nothing about this file beside the MD5
                # Unknown means we need to upload the file  next time we ask
                # Matched verdict means that it is known in some Database 
                updateFileMetadata( status = 'known', verdict='matched', fileID=fileMetadata['_id'], sandblastReputation = queryResult)
                fileStatus = 'known'
                fileVerdict = 'matched'
        #if teStatus == glossary.FOUND:
         #       verdict = queryResult['te']['combined_verdict'].lower()
         #       print '>> File is resolved by Threat Emulation CACHE, with verdict: ' + verdict 
         #       updateFileMetadata( status = 'processed', verdict=verdict, fileID=fileMetadata['_id'], sandblastReputation = queryResult)
         #       fileStatus = 'processed' #by default
                #fileStatus = 'forced' # Forced to be emulated, by default it will stop if te founds it the first time
                # this should be an option at the gui
          #      fileVerdict = verdict
        
        # 2nd Phase - Uploading to Threat Emulation 
        #==============================================
        # - xxOnly MD5 is sent to the API
        # - xxNot dependant of the type of the file since this is only a reputation query
        # - xxif found AV will respond with the info of the malware.
        # - xxif found TE will respond with the forensics report information and the verdicts
        #       even when we sent no file     
                
        while fileStatus != 'processed':
                logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TED'}
                if maxTries == 0:
                        mongoLogger.info(msg.TRY_EXHASUTED,extra=logExtraMetadata)
                        updateFileMetadata( status = 'timeout' )
                        break
                
                mongoLogger.info(msg.TRY_STATUS(tries=maxTries, status=fileStatus),extra=logExtraMetadata)
                teRequest = buildRequest(querytype = 'te', fileMetadata=fileMetadata )                            
               
                if fileStatus in ['unknown', 'known', 'upload_error']:
                        if fileStatus == 'unknown':
                                print '>> File is NOT FOUND on AV nor TE reputation DB.'
                        elif fileStatus == 'known':
                                print '>> File is FOUND on AV but UNKNOWN for TE analysis'
                        elif fileStatus == 'upload_error':
                                print '>> Retry uploading the file...'
                        print '>> 2nd. phase SANDBOX EMULATION'
                        print '>> File will be uploaded to the sandbox'
                        avStatus, teStatus, texStatus, cookie, queryResult = execAPIRequest( request=teRequest,
                                                                        cookie=cookie, apiep='upload', fileID=fileMetadata['_id'], feature='te' )
                        if teStatus == 1002:
                                fileStatus = 'pending'
                                updateFileMetadata( status = 'pending', sandblastEmulation = queryResult )
                                mongoLogger.info(msg.UPLOAD_SUCCESFULLY,extra=logExtraMetadata)
                                # We give just 2 secconds to give static analysis some chance to run before
                                time.sleep(30)

                        elif teStatus == glossary.FOUND:
                                verdict = queryResult['te']['combined_verdict'].lower()
                                updateFileMetadata( status = 'processed', verdict=verdict, fileID=fileMetadata['_id'], sandblastEmulation = queryResult )
                                mongoLogger.info(msg.TE_FOUND(verdict=verdict, tries=maxTries),extra=logExtraMetadata)
                                fileStatus = 'processed' 
                                fileVerdict = verdict
                                if verdict == 'malicious':
                                        mongoLogger.info(msg.FETCH_FORENSICS_REPORTS,extra=logExtraMetadata)
                                        md5 = fileMetadata['md5']
                                        fetchForensicsReports(cookies = cookie, md5 = md5 )
                        
                        elif teStatus == 1003 or 1006:
                                waitingTime = 30
                                fileStatus = 'pending'
                                updateFileMetadata( status = 'pending', sandblastEmulation = queryResult )
                                mongoLogger.info(msg.FILE_PENDING(tries=maxTries, waitingTime=waitingTime),extra=logExtraMetadata)
                               
                                fileStatus = 'pending' 
                                time.sleep(waitingTime)

                                
                        else:
                                fileStatus = 'upload_error'
                                updateFileMetadata( status = 'upload_error' )
                                mongoLogger.info(msg.UPLOAD_ERROR(tries=maxTries),extra=logExtraMetadata)
                                time.sleep(3)
                                
                                             
                                        
                elif fileStatus == 'pending' :
                        mongoLogger.info(msg.ASK_STATUS,extra=logExtraMetadata)
                        
                        avStatus, teStatus, texStatus, cookie, queryResult = execAPIRequest( request=teRequest, cookie=cookie, apiep='upload', feature='te')
                        
                        if teStatus == glossary.FOUND:
                                verdict = queryResult['te']['combined_verdict'].lower()
                                updateFileMetadata( status = 'processed', verdict=verdict, fileID=fileMetadata['_id'], sandblastEmulation = queryResult )
                                mongoLogger.info(msg.TE_FOUND(verdict=verdict, tries=maxTries),extra=logExtraMetadata)
                                fileStatus = 'processed' 
                                fileVerdict = verdict
                                if verdict == 'malicious':
                                        mongoLogger.info(msg.FETCH_FORENSICS_REPORTS,extra=logExtraMetadata)
                                        md5 = fileMetadata['md5']
                                        fetchForensicsReports(cookies = cookie, md5 = md5)
                        else:
                                time.sleep(30)
               
                                
                #print queryResult
                maxTries = maxTries-1
                
   

def threatExtraction(fileMetadata=None):
        ''' 
                Build a query to be sent with the file
                Send the file and the query on the same request
                
                while TEX is different of FOUND
                        wait 10 seconds
                        retry the same query without sending the file
                once TEX is FOUND then
                        fetch the extracte files
        '''
        maxTries = 10
        cookie = None
        texStatus = 0
        fileStatus = 'unknown'
        logExtraMetadata = {'sampleID': fileMetadata['md5'], 'feature': 'TEX'}
        
        mongoLogger.info(msg.START_TEX_THREAD(tries=maxTries),extra=logExtraMetadata)
        texRequest = buildRequest(querytype = 'tex', fileMetadata=fileMetadata )
        mongoLogger.info(msg.TEX_REQUEST(request=texRequest),extra=logExtraMetadata)
        
        while fileStatus != 'processed':
                if maxTries == 0:
                        mongoLogger.info(msg.TRY_EXHASUTED,extra=logExtraMetadata)
                        updateFileMetadata( status = 'timeout' )
                        break
                
                mongoLogger.info(msg.TRY_STATUS(tries=maxTries, status=fileStatus),extra=logExtraMetadata)
                
                avStatus, teStatus, texStatus, cookie, queryResult = execAPIRequest( request=texRequest, cookie=cookie,
                                                                                            apiep='query', feature='tex')
                mongoLogger.debug(msg.TEX_REQUEST_STATUS(texStatus=texStatus, response=queryResult),extra=logExtraMetadata)
                
                if texStatus == 1004:
                        fileStatus = 'not_found'
                        log = '''file is not found on scrubbing center, please upload it'''
                        mongoLogger.info(log,extra=logExtraMetadata)
                        avStatus, teStatus, texStatus, cookie, queryResult = execAPIRequest( request=texRequest, cookie=cookie, apiep='upload',
                                                                            fileID=fileMetadata['_id'], feature='tex')
                        mongoLogger.debug(msg.TEX_REQUEST_STATUS(texStatus=texStatus, response=queryResult),extra=logExtraMetadata)
                        time.sleep(15)
                        
                elif texStatus == 1002:
                        fileStatus = 'uploaded_succesfully'
                        #updateFileMetadata( status = 'pending', sandblastExtraction = queryResult )
                        log= 'File was SUCCESSFULLY uploaded to SCRUBBING CENTER, I will wait 15 seconds before the next query'
                        mongoLogger.info(log,extra=logExtraMetadata)
                        # We give just 10 secconds to allow extraction
                        time.sleep(15)
                        
                elif texStatus == 1003:
                        fileStatus = 'pending'
                        #updateFileMetadata( status = 'pending', sandblastExtraction = queryResult )
                        log= 'File wis Pending, I will wait 15 seconds before the next query'
                        mongoLogger.info(log,extra=logExtraMetadata)
                        # We give just 10 secconds to allow extraction
                        time.sleep(15)
                        
                elif texStatus == 1001:
                        #updateFileMetadata( status = 'processed', verdict=verdict, fileID=fileMetadata['_id'], sandblastEmulation = queryResult )
                        log = ''' Scrubbed content is FOUND .. Updating file metadada'''
                        mongoLogger.info(log,extra=logExtraMetadata)
                        fileStatus = 'processed'
                        updateFileMetadata( status = 'scrubbed', fileID=fileMetadata['_id'], sandblastExtraction = queryResult )

                        #mongoLogger.info(msg.FETCH_FORENSICS_REPORTS,extra={'sampleID': fileMetadata['md5']})
                        #md5 = fileMetadata['md5']
                        fetchExtractedfile( cookies = cookie, md5 = fileMetadata['md5'])
                        
                maxTries = maxTries-1
        return 0
         
def buildRequest(querytype = None, fileMetadata = None ):
        '''Returns a Query for different querytypes:
        'avte' query type will use 'av','te' features and will ask only for reputation using MD5
        'te' query will use 'te','teeb' features and will be used for uploading the file and
                to query the status of a file.
        'tex' query type will use 'extraction' feature and will be the same for uploading the file
                and to query the status of the file.
        '''
    
        #print fileMetadata
        
        query = {}
        if querytype == 'av':
        # If the policy is set to no_enabled, then abort the routine
                query['features'] = ['av']
                #query['te'] = {'reports':['pdf']}
        
        elif querytype == 'te':
                query['features'] = ['te']
                #query['additional_atributes'] = { "force_emulation" : "true"}
                query['te'] = {"reports":['pdf','xml','tar'],
                                "additional_attributes":{ "force_emulation" : True},
                                "images": [##{"id": "7e6fe36e-889e-4c25-8704-56378f0830df","revision": 1}, # Win7,Office 2003/7,Adobe 9
                                          {"id": "e50e99f3-5963-4573-af9e-e3f4750b55e2","revision": 1}, #  WinXP,Office 2003/7,Adobe 9
                                          {"id": "8d188031-1010-4466-828b-0cd13d4303ff","revision": 1}, # Win7,Office 2010,Adobe 9.4
                                          {"id": "10b4a9c6-e414-425c-ae8b-fe4dd7b25244","revision": 1}]} # Win10 64b,Office 2016,Adobe DC
                # images[ "images"] =[{"id": "7e6fe36e-889e-4c25-8704-56378f0830df","revision": 202}, # Win7,Office 2003/7,Adobe 9
                #                   {"id": "e50e99f3-5963-4573-af9e-e3f4750b55e2","revision": 202}, #  WinXP,Office 2003/7,Adobe 9
                #                   {"id": "8d188031-1010-4466-828b-0cd13d4303ff","revision": 202}, # Win7,Office 2010,Adobe 9.4
                #                   {"id": "10b4a9c6-e414-425c-ae8b-fe4dd7b25244","revision": 202}] # Win10 64b,Office 2016,Adobe DC
        elif querytype == 'tex':
                query['features'] = ['extraction']
                query['extraction'] = {"method": "pdf"}
                
        query['md5']  = fileMetadata['md5']
        query['file_name']=fileMetadata['filename']
        return {"request": [query]}
        
        
        
def execAPIRequest( request=None, cookie="EFPHKIMA", fileID=None, apiep=None, feature=None):
        '''execAPIRequest will sent a query to an API Service, one primary and one backup service can
        be defined, if the primary fails, the second one will fail over when a connection error is raised,
        this routine will select when the service is cloud based and need to deliver an API Key and needs
        to handle a cookie.
        this routine must receive the query and the analysis location
        Params:
                requests : the query that was built by buildQuery
                cookie : This mantain persistency with one appliance, used for BOTH clouds, since
                         the local haproxy will insert the hostname as a cookie
                fileID : if the previous response was not found, then we will upload the file
                apiep:  API EndPoint can be quota, upload, query or download and will select the method
                        and the right way to send the request
                featire: av, te, tex, this is used for switches but no when constructing queries.
        '''
        #cookie = {}
        serviceSettings = loadSettings()
        servicePrimary={}
        serviceBackup={}
        headers = {}
        headers["Authorization"] = serviceSettings['sbapi_token']
       
        
        if apiep == 'quota':
                quotaResults = requests.get('''https://te.checkpoint.com/tecloud/api/v1/file/quota''', headers = headers, verify = False)
                return quotaResults
        
        elif apiep == 'download':
                if feature == 'te':
                        # fetchForensicsReport will retrieve the file from the cloud, write it to
                        # GridFS and update the metadata related with that report.
                        print '>> Fetching FORENSICS reports.'
                        try:
                            fetchForensicsReports(images=None, cookies=None, md5=None)    
                        except  requests.exceptions.ConnectionError:
                                downloadURL = serviceSettings['service_primary'] + 'download'
                                params = {'id': 'id_de_reporte'}
                                results = requests.get( downloadURL, headers=headers, params=params, cookies = cookie, verify=False)
           
           
           
        
        elif apiep == 'upload' or 'query':
                if cookie == None:
                        print 'xx'
                        #mongoLogger.info(msg.NO_COOKIE,extra={'sampleID': request['request']['md5']})
                else:
                        print 'xxx'
                        #mongoLogger.info(msg.THREREIS_COOKIE(cookie=cookie),extra={'sampleID':request['request']['md5'] })
                        
                try:
                        print '>> Executing query on PRIMARY service: ' + serviceSettings['service_primary'] 
                        if fileID is  None:
                                headers["Content-Type"] = 'application/json'
                                print '>> Reputation request for file : xxx '
                                # If we recive a file then we should not use query but upload endpoint
                                # if primary location is cloud, the request will include token and cookies
                                #headers["Authorization"] = serviceSettings['sbapi_token']
                                results = requests.post( serviceSettings['service_primary'] + 'query', headers = headers,
                                                        data = json.dumps(request), cookies=cookie, verify=False)
                                ##print results.text
                                cookie = results.cookies
                                print results.cookies
                                ##print dir(results.cookies)
                        else:
                                #headers["Authorization"] = serviceSettings['sbapi_token']
                                #print fileID
                                with repofs.get(fileID) as filesample:
                                        print filesample
                                        files = {'file':(filesample),'request': json.dumps(request)}
                                        print ">> Upload file fo Emulation request: " + filesample.filename 
                                        results = requests.post( serviceSettings['service_primary'] + 'upload', headers = headers,
                                                                files=files, cookies=cookie, verify=False)
                                        cookie = results.cookies
                                        emulationResponse = json.loads(results.text)
                                 #       print emulationResponse
                
                except  requests.exceptions.ConnectionError:
                        print '>> Primary service failed.\nExecuting query on BACKUP service: ' + serviceSettings['service_backup']
                        # if connectios fails it will fail over the local gateway API
                        logging.debug( messages.FAILOVER_ACTION( serviceSettings['service_backup'] ) )
                        if fileID is  None:
                                headers["Content-Type"] = 'application/json'
                                # If we recive a file then we should not use query but upload endpoint
                                # if primary location is cloud, the request will include token and cookies
                                results = requests.post( serviceSettings['service_backup'] + 'query', headers = headers, data = json.dumps(request),
                                        cookies=cookie, verify=False)
                                cookie = results.cookies
                        else:
                                with repofs.get(fileID) as filesample:
                                        files = {"file":(filesample),"request": json.dumps(request)}
                                        print "> Emulation reques for file: " + filesample.filename 
                                        print '> Request for uploading a file with content-type multiform/metadata'
                                        results = requests.post( serviceSettings['service_backup'] + 'upload', headers = headers,
                                                                cookies=cookie, files = files, verify=False)
                                        cookie = results.cookies
                                        emulationResponse = json.loads(results.text)
                                  #      print emulationResponse                     
                       
        #print results.text        
        logging.debug( results.text )
        try:
                queryResult = json.loads(results.text)['response'][0]
        except KeyError,TypeError:
                queryResult = json.loads(results.text)['response']
                
        #print cookie
        
        if apiep == 'query':
                try:
                        avStatus = queryResult['av']['status']['code']
                except KeyError:
                        avStatus = None

                try:
                        teStatus = queryResult['te']['status']['code']
                except KeyError:
                        teStatus = None
                
                try:
                        texStatus = queryResult['extraction']['status']['code']
                except KeyError:
                        texStatus = None
        
                
        if apiep == 'upload':
                avStatus = 'Not For Upload'
                try:
                        teStatus = queryResult['te']['status']['code']
                except KeyError:
                        teStatus = None  

                try:
                        texStatus = queryResult['extraction']['status']['code']
                except KeyError:
                        texStatus = None
   
        logging.debug( messages.QUERY_RESULTS(avStatus, teStatus, texStatus, 'xxx') )
        return avStatus, teStatus, texStatus, cookie, queryResult

def getQuota():
        quota = execAPIRequest(apiep = 'quota')
        #print quota.text
        #return quota.json
        #quota = requests.get(self.sbapi_server + 'quota', headers = headers, verify=False)
        #return dict(quota.json())['response'][0]    

        
def fetchExtractedfile(cookies=None, md5=None):
        fileMetadata = db.fs.files.find_one({"md5":md5})
        serviceSettings = loadSettings()
        scrubbedID = fileMetadata['sandblast_extraction']['extraction']['extracted_file_download_id']
        headers = {}
        headers["Authorization"] = serviceSettings['sbapi_token']
        metadata = {}
        logExtraMetadata = {'sampleID': md5, 'feature': 'TEX'}

        downloadURL = "https://te.checkpoint.com/tecloud/api/v1/file/download"
                
        try:
                params = {'id': scrubbedID}
                log =  "Fething scruubed file ID: " + params['id'] + ' from ' + downloadURL #+ ' using existing COOKIE: ' + cookies     
                mongoLogger.info(log,extra=logExtraMetadata)
                req = requests.get( downloadURL, headers=headers, params=params, cookies = cookies, verify=False)
                with repofs.new_file() as scrubbedFile:
                    for chunk in req.iter_content(chunk_size=1024):
                        if chunk:  # filter out keep-alive new chunks
                            scrubbedFile.write(chunk)
                filename=fileMetadata['sandblast_extraction']['extraction']['extraction_data']['output_file_name']
                scrubbedFile.filename = filename
                metadata = {'scrubbed_id': scrubbedID ,
                            'content-type':'application/pdf','file_md5':md5}
                db.fs.files.update({'_id': scrubbedFile._id}, {'$set': metadata })
        except KeyError:
                log =  "There is no file for id : xxx "         
                mongoLogger.info(log,extra=logExtraMetadata)        
        return 0


def fetchForensicsReports(cookies=None, md5=None):
        fileMetadata = db.fs.files.find_one({"md5":md5})
        images = fileMetadata['sandblast_emulation']['te']['images']
        serviceSettings = loadSettings()
        headers = {}
        headers["Authorization"] = serviceSettings['sbapi_token']
        metadata = {}
        logExtraMetadata = {'sampleID': md5, 'feature': 'TED'}

        
        #print images
        #print cookies
        #print md5
        
        #headers = {'Authorization': sbapi_token}
        for image in images:
                log =  "Fething SandBox Forensics reports"
                mongoLogger.info(log,extra=logExtraMetadata)
                downloadURL = "https://te.checkpoint.com/tecloud/api/v1/file/download"
                #PDF Report
                try:
                        params = {'id': image['report']['pdf_report']}
                        log =  "Fething PDF Report with ID: " + params['id'] + ' from ' + downloadURL #+ ' using existing COOKIE: ' + cookies     
                        mongoLogger.info(log,extra=logExtraMetadata)
                        req = requests.get( downloadURL, headers=headers, params=params, cookies = cookies, verify=False)
                        with repofs.new_file() as forensics_report:
                            for chunk in req.iter_content(chunk_size=1024):
                                if chunk:  # filter out keep-alive new chunks
                                        forensics_report.write(chunk)
                        forensics_report.filename = '%s.pdf' % (image['report']['pdf_report'])
                        metadata = {'report_id': image['report']['pdf_report'] ,
                                'content-type':'application/pdf','malwr_md5':md5}
                        db.fs.files.update({'_id': forensics_report._id}, {'$set': metadata })
                except KeyError:
                        log =  "There is no PDF report for : xxx "         
                        mongoLogger.info(log,extra=logExtraMetadata)
                
                #XML Report        
                try:
                        params = {'id': image['report']['xml_report']}
                        log =  "Fething XML Report with ID: " + params['id'] + ' from ' + downloadURL #+ ' using existing COOKIE: ' + cookies     
                        mongoLogger.info(log,extra=logExtraMetadata)
                        req = requests.get( downloadURL, headers=headers, params=params, cookies = cookies, verify=False)
                        with repofs.new_file() as forensics_report:
                            for chunk in req.iter_content(chunk_size=1024):
                                if chunk:  # filter out keep-alive new chunks
                                        forensics_report.write(chunk)
                        forensics_report.filename = '%s.xml' % (image['report']['xml_report'])
                        metadata = {'report_id': image['report']['xml_report'] ,
                                'content-type':'application/xml','malwr_md5':md5}
                        db.fs.files.update({'_id': forensics_report._id}, {'$set': metadata })
                except KeyError:
                        log =  "There is no XML report for :xxx "        
                        mongoLogger.info(log,extra=logExtraMetadata)
                        
                 #TAR Report        
                try:
                        params = {'id': image['report']['tar_report']}
                        log =  "Fething TGZ Report with ID: " + params['id'] + ' from ' + downloadURL #+ ' using existing COOKIE: ' + cookies     
                        mongoLogger.info(log,extra=logExtraMetadata)
                        req = requests.get( downloadURL, headers=headers, params=params, cookies = cookies, verify=False)
                        with repofs.new_file() as forensics_report:
                            for chunk in req.iter_content(chunk_size=1024):
                                if chunk:  # filter out keep-alive new chunks
                                        forensics_report.write(chunk)
                        forensics_report.filename = '%s.tgz' % (image['report']['tar_report'])
                        metadata = {'report_id': image['report']['xml_report'] ,
                                'content-type':'application/tgz','malwr_md5':md5}
                        db.fs.files.update({'_id': forensics_report._id}, {'$set': metadata })
                except KeyError:
                        log =  "There is no TGZ report for :xxx "       
                        mongoLogger.info(log,extra=logExtraMetadata)
            
        return 0



def analysisEvent(sampleid=None):
        # Create an Analysis ID, all responses file will be attached to this analysisID
        starting_timestamp = time.time()
        analysisID= uuid.uuid4()
        print "Log:  new analysis event created with ID: " + str(analysisID)
        analysisEvent = {}
        analysisEvent["analysis_id"] = analysisID
        analysisEvent["starting_timestamp"] = str(starting_timestamp)
        analysisEvent["ending_timestamp"] = 'xxxx'
        analysisEvent["scanning_policy"] = 'xxxx'
        analysisEvent["sample_md5"] = sampleid
        db.analysis.insert(analysisEvent)
        return analysisID