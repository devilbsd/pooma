import requests
import json
import threading
import time
import base64


def analyze(malware_name=None):
	'''Upload file to Metadefender
	'''
	headers = {}
	headers['apikey'] = 'ca24575740a6ca1cc69f24faf0c41392'
	#headers['content-type'] = 'multipart/form-data'
	headers['content-type'] = 'application/octet-stream'
	headers['filename'] = malware_name
	url = "https://api.metadefender.com/v2/file"
								
	opsw_msg = "Request Headers: " + str(headers)
	print opsw_msg
	
	#files = {'file': open('malware.exe', 'r')}
	data = open(malware_name, 'rb').read()

	results = requests.post( url=url, data=data, headers = headers)
			
	opsw_msg = "Request Response: " + results.text
	print opsw_msg
	print results
			
			
def removeDots(scans=None):

	try:
		drweb = scans["Dr.Web Gateway"]
		scans.pop("Dr.Web Gateway")
		scans["Dr_Web Gateway"] = drweb
	except KeyError:
		pass
	try:
		virit = scans["Vir.IT eXplorer"]
		scans.pop("Vir.IT eXplorer")
		scans["Vir_IT eXplorer"] = virit
	except KeyError:
		pass
	try:
		virit = scans["Vir.IT ML"]
		scans.pop("Vir.IT ML")
		scans["Vir_IT ML"] = virit
	except KeyError:
		pass
	return scans




analyze(malware_name='malware.exe')
