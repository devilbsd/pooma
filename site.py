import config
import pooma
import glossary
import messages
import messages as msg
import cherrypy
from cherrypy.lib.static import serve_file
from cherrypy.lib.static import serve_fileobj
from Cheetah.Template import Template
import threading
import json
import os
import requests
import time
import hashlib
import base64
import tempfile 
from pymongo import MongoClient
import pymongo
import gridfs
import magic
import zipfile
from zipfile import BadZipfile
import zlib
import hexdump
import sandblast_fabric
from fabric.api import env
import smtplib
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import logging
import imaplib
import email
import uuid
from log4mongo.handlers import MongoHandler
import metadefender
import crawlers
from phishing import Phishing
from urlparse import urlparse
import random
import pandas as pd
from io import StringIO
from datetime import datetime


#from log4mongo.handlers import MongoHandler

class Root:
    client = MongoClient(config.mongodburi)
    db = client.pooma
    repofs = gridfs.GridFS(db)
    settings = db.settings.find()
    
    ## MongoDB hosted @ Atlas
    atlasClient = MongoClient(config.atlasdburi)
    reputationdb = client.ReputationDB

    mongoLogger = logging.getLogger('PooMALog')
    mongoHandler = MongoHandler(host=config.mongodburi,
                            database_name='pooma',
                            collection='logs')
    mongoLogger.addHandler(mongoHandler)
    
 #   mongoHandler = MongoHandler(host='10.10.10.10',
  #                          database_name='pooma',
  #                          collection='logs')
  #  mongoLogger = logging.getLogger('PooMALog')
  #  mongoLogger.addHandler(mongoHandler)

    #for setting in settings:
     #   if setting['section'] == 'cpsb_cloud':
    sbapi_token  =  'xx'#setting['sbapi_token']
    sbapi_server =  'xx' #setting['sbapi_server']
     #   elif setting['section'] == 'virustotal':
      #      vtapi_token  =  setting['vtapi_token']
       #     vtapi_server =  setting['vtapi_server']
    
    # -- HTML Templates
    siteIndex = Template(file="webui/templates/index.html")
   
    
    def getchksums(self, sample_id=None):
        '''Returns the SHA1 hash from a file. It will work for big and small files.
        They will be read in 128 bytes chunks, so the hashing will happen for a stream
        of chunks, no need to allocate the entire file on RAM.
        shafile is the full or name of the file, defauls to None and is mandatory, '''
        with self.repofs.get(sample_id) as f:
            #sha224_hash  = hashlib.md5
            sha1_hash = hashlib.sha1()
            sha256_hash = hashlib.sha256()
            sha512_hash = hashlib.sha512()
            while True:
                data =  f.read(2**20)
                if not data:
                    break
                #md5_hash.update(data)
                sha1_hash.update(data)
                sha256_hash.update(data)
                sha512_hash.update(data)
            return {'sha1':sha1_hash.hexdigest(), 'sha256':sha256_hash.hexdigest(), 'sha512':sha512_hash.hexdigest()}
    
    @cherrypy.expose
    def login(self):
        sectionLogin = Template(file="webui/templates/sectionLogin.html")
        self.siteIndex.menu= glossary.menu_default
        self.siteIndex.content = str(sectionLogin)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def about(self):
        sectionAbout = Template(file="webui/templates/sectionAbout.html")
        self.siteIndex.menu= glossary.menu_default
        self.siteIndex.content = str(sectionAbout)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def index(self):
        sectionPooma = Template(file="webui/templates/sectionPooma.html")
        sectionSendFile = Template(file="webui/templates/sectionSendFile.html")
        sectionFetchFile = Template(file="webui/templates/sectionFetchFile.html")
        widgetHashLookUp = Template(file="webui/templates/widgetHashLookUp.html")
        widgetURLRep = Template(file="webui/templates/widgetIPURLRep.html")
        #hashes = self.db.fs.hashes({ 'status': { '$exists': True } }).sort('uploadDate',-1).limit(10)
        hashes = self.db.hashlist.find({ 'sampleHash': { '$exists': True } }).sort('_id',-1).limit(10)
        ipurls = self.db.ipurl.find({ 'url_id': { '$exists': True } }).sort('_id',-1).limit(10)
        samples = self.db.fs.files.find({ 'status': { '$exists': True } }).sort('uploadDate',-1).limit(10)
        sectionPooma.upload_file = str(sectionSendFile)
        sectionPooma.fetch_file = str(sectionFetchFile)
        sectionPooma.widget_hash = str(widgetHashLookUp)
        sectionPooma.widget_ipurl = str(widgetURLRep)
        sectionPooma.last_hash_lookups = hashes
        sectionPooma.last_ipurls_lookups = ipurls
        sectionPooma.samples = samples
        self.siteIndex.menu= glossary.menu_default
        self.siteIndex.content = str(sectionPooma)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def tags(self):
        '''tags - this endpoint will build a dict of relevant samples:
        tag_dict = {
                'venus locker': [ {'Malware.Name.A':'MD5 HASH'},
                                  {'Malware.Name.B':'MD5 HASH'} ],
                'shell locker': [ {'Malware.Name.C':'MD5 HASH'},
                                  {'Malware.Name.D':'MD5 HASH'} ]
            }
        '''
        sectionTag = Template(file="webui/templates/sectionTags.html")
        tag_list = []
        tag_dict = {}
        
        for files in self.db.fs.files.find({ 'tags': { '$exists': True } }):
            for tag in files['tags']:
                tag_list.append(tag)
            set(tag_list)
            
        for tag in set(tag_list):
            print tag
            malware_list = []
            for sample in self.db.fs.files.find({ 'tags': { '$exists': True } }):
                if tag in sample['tags']:
                    malware_list.append({sample['filename']:sample['md5']})
                    #malware_list.append({sample['sandblast'][0]['av']['malware_info']['signature_name']:sample['md5']})
                tag_dict[tag]=malware_list      
        sectionTag.tag_dict = tag_dict
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionTag)
        return str(self.siteIndex)
    

    
    
    #-------            SETTINGS SECTION        --------#
    @cherrypy.expose
    def database(self):
        sectionDB = Template(file="webui/templates/sectionDB.html")
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionDB)
        return str(self.siteIndex)
    
    def loadSettings(self):
        return self.db.settings.find_one({"section": "sandblast"})

    
    @cherrypy.expose
    def saas(self):
        sectionSaaS = Template(file="webui/templates/sectionSaaS.html")
        sectionSaaS.emails_settings = self.db.settings.find({'setting_type':'email'})
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionSaaS)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def crawlers(self):
        widgetHashList = Template(file="webui/templates/widgetCrawlHashFile.html")
        widgetURLList = Template(file="webui/templates/widgetCrawlURLFile.html")
        widgetYTD = Template(file="webui/templates/widgetYouTubeDwld.html")
        sectionCrawlers = Template(file="webui/templates/sectionCrawlers.html")
        
        ## FTP Crawler
        sectionCrawlers.settings = self.db.settings.find({'setting_type':'ftpcrawler'})
        sectionCrawlers.widget_hashlist = str(widgetHashList)
        sectionCrawlers.widget_urllist = str(widgetURLList)
        sectionCrawlers.widgetYTD = str(widgetYTD)
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionCrawlers)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def editEmailAccount(self, email = None):
        sectionEditEmail = Template(file="webui/templates/sectionEditEmailAccount.html")
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionEditEmail)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editIMAPCrawler(self, email = None):
        sectionIMAPCrawler = Template(file="webui/templates/sectionIMAPCrawler.html")
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionIMAPCrawler)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editFTPCrawler(self, crawler = None):
        sectionCrawler = Template(file="webui/templates/sectionEditFTPAccount.html")
        
        if crawler is None:
            ## If this is a new crawler then create a new user ID and add a defult policy for
            ## the creation step
            crawler = uuid.uuid4()
            sectionCrawler.newaccount = 1
            ## Defatul creation settings
            crawlerSettings = {}
            sectionCrawler.crawlerSettings = crawlerSettings
        else:
            ## If this is a known crawler go to DB and find it's settings)
            sectionCrawler.newaccount = 0
            crawlerSettings = self.db.settings.find_one({'crawlerid':crawler})
            sectionCrawler.crawlerSettings = crawlerSettings
            
        sectionCrawler.crawlerid = crawler
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionCrawler)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def viewMessages(self, mail = None):
        msgs = []
        sectionViewMessages = Template(file="webui/templates/sectionViewMessages.html")
        webfaction = 'mail.webfaction.com'
        eMail = imaplib.IMAP4(webfaction)
        eMail.login('pooma','x3w9db7tf54')
        eMail.select(readonly=1)
        typ, msgnums = eMail.search(None, "ALL")
        for num in msgnums[0].split():
            #emailmsg = {}
            typ, msgnums = eMail.fetch(num, '(RFC822)')
            msg = email.message_from_string(msgnums[0][1])
            #emailmsg['from'] = msg['From']
            #emailmsg['subject'] =
            print msg.is_multipart()
            for part in msg.walk():
                print part.get_content_type()
                print part.get_filename()
                #print part.is_multipart()
                msgs.append(part.items())
            print '\n'    
            #msgs.append('Message %s\n%s\n' % (num, msgnums[0][1]))
            
        sectionViewMessages.msgs = msgs
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionViewMessages)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def youtubedwld (self, media_format = None, youtube_url = None):
        ytmedia = { "url": youtube_url,
                    "media_format": media_format}
        
        result_insert = self.db.youtubedwld.insert_one(ytmedia)
        return self.crawlers()
    
    
    @cherrypy.expose
    def saveEmailAccount(self, email = None, user = None, passwd = None, service = None, mode = None, status = None,
                         imap_host = None, imap_port = None, smtp_host = None, smtp_port = None):
        email_settings = {}
        email_settings['email'] = email
        email_settings['user'] = user
        email_settings['passwd'] = passwd
        email_settings['service'] = service
        email_settings['mode'] = mode
        email_settings['status'] = status
        email_settings['enabled'] = True
        email_settings['setting_type'] = 'email'
        result_find = self.db.settings.insert_one(email_settings)
        return self.editEmailAccount( email = email )
                                     

    @cherrypy.expose
    def saveFTPAccount(self, crawlerid = None, user = None, password = None, port = None,  customport = 21,
                       servicename = None, hostname = None, protection_mode = "monitor", maxfilesize = 10, folderdeep = 5, status = None):
        settings = {}
        settings['crawlerid'] = crawlerid
        settings['servicename'] = servicename
        settings['hostname'] = hostname
        settings['user'] = user
        settings['passwd'] = password
        settings['port'] = port
        settings['customport'] = customport
        settings['enabled'] = "true"
        settings['status'] = "waiting"
        settings['protection_mode'] = protection_mode
        settings['setting_type'] = "ftpcrawler"
        settings['folder_deep'] = 5
        results = self.db.settings.update_one( {'setting_type':'ftpcrawler','crawlerid':crawlerid },
                                               {'$set': settings}, upsert = True)
        
        ## Test Connection to verify Account and update it status
        ftpMessage = crawlers.test_ftp_connection(crawler = crawlerid)
        results = self.db.settings.update_one( {'setting_type':'ftpcrawler','crawlerid':crawlerid },
                                               {'$set': {'status': ftpMessage }})
        return self.crawlers( )
        
    
    @cherrypy.expose
    def feeds(self):
        sectionFeeds = Template(file="webui/templates/sectionFeeds.html")
        from haproxystats import HAProxyServer
        # #Get Feed Settings
        # result_find = self.db.settings.find({'setting_type':'feed'})
        # feed_settings = []
        # for setting in result_find:
        #     feed_settings.append(setting)
        
        # - Load SandBlast Feed configuration
        #   - Service HA Settings
        #   - Emulation Environments
        # -----------------------------------------------------------------
        
        sectionFeeds.globalSettings = self.loadSettings()
        print self.loadSettings()
        
        
        # - Load SandBlast Public Cloud settings
        #   - TP Key
        
        sectionFeeds.sandblast_cloud_settings = self.db.settings.find_one({'service_type':'public_cloud', 'feed_name':'sandblast'})
        #     
        # result_find = self.db.settings.find({'setting':'feed'})
        # feeds = []
        # for feed in result_find:
        #     feeds.append(feed)
        #     
        # #Get nodes from private cloud
        # result_find = self.db.settings.find({'setting_type':'node'})
        # pvcNodes = []
        # for node in result_find:
        #     pvcNodes.append(node)
        
        # sectionFeeds.feed_settings = feed_settings[0]
        # sectionFeeds.pvcNodes = pvcNodes
        
        # - SandBlast Private Cloud configuration
        #   - Show status of each node
        #   - Add/Edit/Delete nodes
            
        # - HAPROXY Statistics for Private Cloud
   
        haproxy = HAProxyServer('10.0.0.140' + ':1936')
        print json.loads(haproxy.to_json())
        sectionFeeds.haproxy_csvfile = json.loads(haproxy.to_json())
        sectionFeeds.proxy_key = '10.0.0.140'
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionFeeds)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def features(self):
        sectionFeeds = Template(file="webui/templates/sectionFeatures.html")
        result_find = self.db.settings.find({'setting':'feed'})
        feeds = []
        for feed in result_find:
            feeds.append(feed)
        sectionFeeds.feeds = feeds
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionFeeds)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editFeed(self, feed = None):
        if feed =='SandBlast':
            settingEditFeed = Template(file="webui/templates/sectionFeatures.html")
        elif feed == 'PublicCloud':
            settingEditFeed = Template(file="webui/templates/settingsSBPublicCloud.html")
            settingEditFeed.feedData = self.db.settings.find_one({'section':'sandblast'})
            #print result_find
            #settingEditFeed.feedData = result_find
        elif feed == 'PrivateCloud':
            settingEditFeed = Template(file="webui/templates/sectionSBPrivateCloud.html")           
            result_find = self.db.settings.find({'setting_type':'node', 'feed_name':'sandblast'})
            sandblast_pvc_nodes = []
            for node in result_find:
                sandblast_pvc_nodes.append(node)
            settingEditFeed.nodes = sandblast_pvc_nodes
            
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(settingEditFeed)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editNode(self, node = None):
        settingEditNode = Template(file="webui/templates/sectionEditNode.html")
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(settingEditNode)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def deleteNode(self, node = None):
        result_delete = self.db.settings.delete_one({ 'hostname': node })
        return self.editFeed(feed = 'PrivateCloud')
    
    @cherrypy.expose
    def saveNode(self, hostname = None, ipv4_addr = None, user = None, passwd = None, cookie = None, status = None):
        node_settings = {}
        sandblast_host = user + '@' + ipv4_addr
        hostname, system = sandblast_fabric.addSandBlast( host = sandblast_host, password = passwd) 
        node_settings['ipv4_addr'] = ipv4_addr
        node_settings['user'] = user
        node_settings['passwd'] = passwd
        node_settings['cookie'] = hostname
        node_settings['hostname'] = hostname
        node_settings['system'] = system
        node_settings['status'] = status
        node_settings['enabled'] = True
        node_settings['setting_type'] = 'node'
        node_settings['feed_name'] = 'sandblast'
        result_find = self.db.settings.insert_one(node_settings)
        return self.editFeed(feed = 'PrivateCloud')
    
    @cherrypy.expose
    def haproxy_cfg(self):
        haproxy_cfg = Template(file="conf_templates/haproxy.cfg")
        result_find = self.db.settings.find({'setting_type':'node', 'feed_name':'sandblast'})
        sandblast_pvc_nodes = []
        for node in result_find:
            sandblast_pvc_nodes.append(node)
        haproxy_cfg.nodes = sandblast_pvc_nodes
        haproxy_cfg.poomaAppIP = config.poomaAppIP
        haproxy_cfg.poomaAppPort = config.poomaAppPort
        haproxy_cfg.poomaFrontIP = config.poomaFrontIP
        haproxy_cfg.sbPrivateCloudIP = config.sbPrivateCloudIP
        return str(haproxy_cfg)
    
    @cherrypy.expose
    def setLBConf(self):
        haproxy_host = 'admin@10.0.0.140'
        output = sandblast_fabric.setHAProxyConfig(host = haproxy_host) 
        #self.siteIndex.menu = glossary.menu_default
        #self.siteIndex.content = str(sectionCmdsOutput)
        return self.feeds()

    #-------            End of SETTINGS SECTION        --------#
    
    #-------            Widgets                        --------#
    @cherrypy.expose
    def repo(self):
        # Get the active filter ti build the query
        activeFilter = self.db.settings.find_one({"section": "widgetFilter", "active":True})
        sectionBenign = Template(file="webui/templates/sectionBenign.html")
        sectionBenign.widgetFilter = self.widgetFilter(section='myfiles')
        #Based on the filter I will build the query
        try:
            tagsList = activeFilter['tags']
        except:
            tagsList = []
            
        try:
            contentList = activeFilter['content']
        except:
            contentList = []

        try:
            sourceList = activeFilter['source']
        except:
            sourceList = []
            
        try:
            statusList = activeFilter['status']
        except:
            statusList = []
            
        try:
            verdictList = activeFilter['verdict']
        except:
            verdictList = []
        
        filterList = []
        
        if len(verdictList) != 0:
            filterList.append({'verdict': { '$in': verdictList }})
            
        if len(contentList) != 0:
            filterList.append({'content-type-mime': { '$in': contentList }})
            
        if len(tagsList) != 0:
            filterList.append({'tags': { '$in': tagsList }})
        
        if len(statusList) != 0:
            filterList.append({'status': { '$in': statusList }})
            
        if len(sourceList) != 0:
            filterList.append({'origin': { '$in': sourceList }})
            
            
        
        
        if activeFilter['logic'] == 'and':
          
            filterQuery = { '$and': filterList}
        else:
            filterQuery = { '$or':[ {'tags': { '$in': tagsList }} ,
                                {'content-type-mime': { '$in': contentList }},
                                {'status': { '$in': statusList }},
                                {'verdict': { '$in': verdictList }},
                                {'origin': { '$in': sourceList }}]}
        
        samples = self.db.fs.files.find(filterQuery).sort('uploadDate',-1)
        sectionBenign.samples = samples
        
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionBenign)
        return str(self.siteIndex)
    
    def widgetFilter(self, section=None):
        activeFilter = self.db.settings.find_one({"section": "widgetFilter", "active":True})
        if activeFilter == None:
            print ''''create default filter'''
            activeFilter = glossary.defaultFilter
            self.db.settings.insert(glossary.defaultFilter)
            
        #samples = self.db.fs.files.find(glossary.VERD_SAFE).sort('uploadDate',-1)
        
        tag_list = []
        for files in self.db.fs.files.find({ 'tags': { '$exists': True } }):
            for tag in files['tags']:
                tag_list.append(tag)
                
        content_list = []
        for files in self.db.fs.files.find({ 'content-type-mime': { '$exists': True } }):
            content_list.append(files['content-type-mime'])
            
        source_list = []
        for files in self.db.fs.files.find({ 'origin': { '$exists': True } }):
            source_list.append(files['origin'])
        
        filter_list = []
        for filterItem in self.db.settings.find({"section": "widgetFilter", "active":False}):
            filter_list.append(filterItem['filtername'])
          
        widget = Template(file="webui/templates/widgetFilter.html")
        widget.edit = activeFilter['edit']
        widget.filterList = filter_list
        widget.activeFilter = activeFilter
        widget.tagList = set(tag_list)
        widget.contentList = set(content_list)
        widget.sourceList = set(source_list)
        return str(widget)
    
    @cherrypy.expose
    def saveFilter(self):
        '''Do a copy of the active filter'''
        activeFilter = self.db.settings.find_one({"section": "widgetFilter", "active":True},{'_id': 0})
        activeFilter['active'] = False
        activeFilter['edit'] = 'false'
        #activeFilter.pop('_id')
        
        self.db.settings.update({"section": "widgetFilter", "active":False, "filtername": activeFilter['filtername']} ,
                                {'$set': activeFilter }, upsert=True )
        return self.repo()
    
    @cherrypy.expose
    def addFilter(self, tag=None, content=None, source=None, filtername=None, status=None, verdict = None, maxItems=None, savedfilter=None,
                  logic=None):
        activeFilter = self.db.settings.find_one({"section": "widgetFilter", "active":True}, {'_id': 0})
        
        if savedfilter == 'None':
            pass
        else:
            savedFilter = self.db.settings.find_one({"section": "widgetFilter", "filtername":savedfilter}, {'_id': 0})
            #savedFilter.pop('_id')
            savedFilter['active'] = True
            #- Get saved fil'ter
            resultsUpdate = self.db.settings.update({'section': "widgetFilter", "active": True}, {'$set': savedFilter}, upsert=True)        
            return self.repo()
      
        activeFilter['filtername'] = filtername
        activeFilter['logic'] = logic
    
        ## --- TAGS LIST       
        if tag == 'None':
            pass
        elif tag == 'clear':
            activeFilter['tags'] =[]   
        else:
            try:
                newList = activeFilter['tags']
                newList.append(tag)
                activeFilter['tags'] = newList
            except:
                activeFilter['tags'] =[]
                
        ## -- CONTENT LIST
        if content == 'None':
            pass
        elif content == 'clear':
            activeFilter['content'] =[]  
        else:
            try:
                newList = activeFilter['content']
                newList.append(content)
                activeFilter['content'] = newList
            except:
                activeFilter['content'] =[]
            
            
        ## --SOURCE LIST    
        if source == 'None':
            pass
        elif source == 'clear':
            activeFilter['source'] =[]  
        else:
            try:
                newList = activeFilter['source']
                newList.append(source)
                activeFilter['source'] = newList
            except:
                activeFilter['source'] =[]
                
         
        ## -- STATUS LIST        
        if status == 'None':
            pass
        elif status == 'clear':
            activeFilter['status'] =[]  
        else:
            try:
                newList = activeFilter['status']
                newList.append(status)
                activeFilter['status'] = newList
            except:
                activeFilter['status'] =[]
                
        ## -- VERDICT LIST        
        if verdict == 'None':
            pass
        elif verdict == 'clear':
            activeFilter['verdict'] =[]  
        else:
            try:
                newList = activeFilter['verdict']
                newList.append(verdict)
                activeFilter['verdict'] = newList
            except:
                activeFilter['verdict'] =[]
    
        resultsUpdate = self.db.settings.update({'section': "widgetFilter", "active": True}, {'$set': activeFilter }, upsert=True)
        
        return self.repo()
     
     
    @cherrypy.expose
    def editFilter(self, enable=None):
        if enable=='true':
            resultsUpdate = self.db.settings.update({'section': "widgetFilter", "active": True}, {'$set': {'edit': 'true'}  }, upsert=True)
        else:
            resultsUpdate = self.db.settings.update({'section': "widgetFilter", "active": True}, {'$set': {'edit': 'false'} }, upsert=True)
        return self.repo()
     
    
    @cherrypy.expose
    def file(self, sampleid=None):
        #sectionSample = Template(file="webui/templates/sectionSample.html")
        sample = self.db.fs.files.find_one({'md5':sampleid})
        evidence = self.db.fs.files.find({"sample_id":sampleid, "origin":"sample-upload"})
        metadefenderMetadata = self.db.analysis.find_one({'malwr_md5': sampleid})

        tag_list = []
        for files in self.db.fs.files.find({ 'tags': { '$exists': True } }):
            for tag in files['tags']:
                tag_list.append(tag)
    
        sectionSample = Template(file="webui/templates/sectionFile.html")
        archivedContent = self.db.fs.files.find({"sample_id":sampleid, "origin":"archived"})
       
        if archivedContent == None:
            sectionSample.archives = 0
        else:
            sectionSample.archives = archivedContent
        sectionSample.tag_list = set(tag_list)
        sectionSample.evidence = evidence
        sectionSample.sample = sample
        sectionSample.metadefenderMetadata = metadefenderMetadata
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionSample)
        return str(self.siteIndex)

    @cherrypy.expose
    def agents(self, script=None, ):
        sectionAgents = Template(file="webui/templates/sectionAgents.html")
        bashAgentScript = Template(file="webui/templates/agentScript.sh")
        downloadAllMalwrScript = Template(file="webui/templates/download_script.sh")
        
        #result_find   = self.db.settings.find_one({'section_name':'agents'})
        
        ## Look for agents 
        response = requests.get( config.xolodMgmtAPI + 'agents' )
        agents  = json.loads(response.text)
        print agents
        sectionAgents.agents = agents     
        
        ##This part wil migratd
        if script == 'bashlinuxbsd':
            return str(bashAgentScript)
        if script == 'download':
            return str(downloadAllMalwrScript)
        #########################################
        
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionAgents)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def pooma_init_agent(self, platform=None):
        if platform == 'bsd':
            sectionScript = Template(file="webui/templates/xolod_init_script.sh")
            return str(sectionScript)
        else:
            sectionScript = Template(file="webui/templates/sectionScripts.html")
            self.siteIndex.menu = glossary.menu_agents
            self.siteIndex.content = str(sectionScript)
            return str(self.siteIndex)
    
    @cherrypy.expose
    def scripts(self, script=None):
        if script == 'mlwrlistmd5':
            md5list= []
            mlwrlist = Template(glossary.md5list_tmpl)
            for sample in self.repofs.find(glossary.VERD_MALICIOUS):
                md5list.append(sample.md5)
            mlwrlist.md5set = set(md5list)  
            return str(mlwrlist)
        else:
            sectionScripts = Template(file="webui/templates/sectionScripts.html")
            self.siteIndex.menu = glossary.menu_agents
            self.siteIndex.content = str(sectionScripts)
            return str(self.siteIndex)
        
    @cherrypy.expose
    def content(self, md5 = None):
        sectionFileContent = Template('''<pre>$filecontent</pre>''')
        sample = self.db.fs.files.find_one({'md5':md5})
        with self.repofs.get(sample["_id"]) as filesample:
            sectionFileContent.filecontent = filesample.read()
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionFileContent)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def csvtohtml(self, md5 = None):
        
        #htmltable = Template('''$filecontent''')
        sample = self.db.fs.files.find_one({'md5':md5})
        csvfile = pd.read_csv(self.repofs.get(sample["_id"]))
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(csvfile.to_html())
        return str(self.siteIndex)

    @cherrypy.expose
    def viewLog(self, sampleID=None): 
        sectionViewLog =Template(file="webui/templates/sectionViewLog.html")
        sectionViewLog.logs = self.db.logs.find({'sampleID':sampleID})
        self.siteIndex.content = str(sectionViewLog)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def hexdump(self, md5 = None):
        sectionFileContent = Template('''<pre>$filecontent</pre>''')
        sample = self.db.fs.files.find_one({'md5':md5})
        with self.repofs.get(sample["_id"]) as filesample:
            filehexdump = hexdump.hexdump(filesample.read())
        self.siteIndex.menu = glossary.menu_agents
        sectionFileContent.filecontent = filehexdump
        self.siteIndex.content = str(sectionFileContent)
        return str(self.siteIndex)
     
    @cherrypy.expose
    def showQuota(self):
        sectionQuota = Template(file="webui/templates/sectionSBCQuota.html")
        sectionQuota.stats = pooma.getQuota()
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionQuota)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def reAnalyzePolicy(self, sampleID = None):
        sectionreAnalizePolicy = Template(file="webui/templates/sectionreAnalizePolicy.html")
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionreAnalizePolicy)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editPolicy(self, policyID = None):
        policyset = self.db.settings.find({"setting_type":"file_analysis_policy"})
        sectionEditPolicy = Template(file="webui/templates/sectionEditPolicy.html")
        sectionEditPolicy.policyset = policyset
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionEditPolicy)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def editAgentPolicy(self, agentID = None):
        policyset = self.db.settings.find({"setting_type":"file_analysis_policy"})
        
        sectionEditAgentPolicy = Template(file="webui/templates/sectionEditAgentPolicy.html")
        sectionEditAgentPolicy.policyset = policyset
        
        ## Look for an specifc agent info
        response = requests.get( config.xolodMgmtAPI + 'agents/'  + agentID )
        agent = json.loads(response.text)
        sectionEditAgentPolicy.agent = agent["Items"][0]
        
        
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(sectionEditAgentPolicy)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def updateAgentPolicy(self, agentID = None, agent_debugging = None, frequency = None, heartbeat_ttl = None,
                           proxyhost = None, proxyport = None, proxyuser = None, proxypwd = None,
                           currentversion = None):
        newPolicy = {
            "exp_date" :"234234234",
            "heartbeat_frequency" : int(frequency),
            "heartbeat_ttl" : heartbeat_ttl,
            "last_update" : 'todays date',
            "version" : int(currentversion) + 1,
            "agent_debugging" : agent_debugging,
            "proxy_host" : proxyhost,
            "proxy_port" : proxyport,
            "proxy_user" : proxyuser,
            "proxy_pwd" : proxypwd
        }
        
        print newPolicy
        api_url = config.xolodMgmtAPI + 'agents/'  + agentID
        print api_url
        response = requests.post( api_url, data=newPolicy )
        print response.text
        self.siteIndex.menu = glossary.menu_agents
        self.siteIndex.content = str(response)
        return str(self.siteIndex)

    @cherrypy.expose
    def policy(self):
        policyset = self.db.settings.find_one({"setting_type":"file_analysis_policy", "default_policy": True})
        sectionPolicy = Template(file="webui/templates/sectionPolicy.html")
        sectionPolicy.policy = policyset
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionPolicy)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def analysisondemand(self):
        sectionPolicy = Template(file="webui/templates/sectionAnalysisOD.html")
        self.siteIndex.menu = glossary.menu_settings
        self.siteIndex.content = str(sectionPolicy)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def setPolicy(self, policyID = None, sampleID = None):
        '''setPolicy will became active a policy identified with policyID,
        - policyID: mandatory
        - sampleID: optional, if specified the policy will only apply to an specific file
                    if it is not specified it will became the deault for all files
        '''
        # Look for the default policy
        default_policy = self.db.settings.find_one({"setting_type":"file_analysis_policy", "default_policy": True})
        # Set default to no default
        resultsUpdate = self.db.settings.update({"setting_type":"file_analysis_policy",
                                                 "policy_id": default_policy['policy_id']},
                                                {'$set': {"default_policy": False}}, upsert=True)
        # Set desire policy to defaul = true
        resultsUpdate = self.db.settings.update({"setting_type":"file_analysis_policy",
                                                 "policy_id": policyID},
                                                {'$set': {"default_policy": True}}, upsert=True)
        return self.policy()

    
    
    
    @cherrypy.expose
    def savePolicy(self, emailPolicy = None,  filePolicy = None, sampleID = None):
        return 0
    
    @cherrypy.expose
    def saveConfig(self, section=None, sbapi_server=None, sbapi_token=None, admin_email=None, admin_password=None, 
                    vtapi_server=None, vtapi_token=None, agent_schedule=None, agent_action=None, target_dirs=None,
                    agent_enc_password=None, setting=None, primary=None, backup=None):
        '''Save configuration settings
        section: name of the section of settings to be modified
            -cpsb_cloud:    SandBlast Cloud API
            -cpsb_local:    SandBlast Local API
                -sbapi_server:  Where the SPI is published
                -sbapi_token:   ID Token'''
        ## SANDBLAST SETTINGS
        if setting == 'ha':
            sboxConf= { 'service_backup': backup,
                       'service_primary': primary,
                       'section':section,
                       'enabled': True}
                        
        elif setting == 'public_cloud_access':
            sboxConf= { 'section':section,
                       'sbapi_token' : sbapi_token}
        
        #result_insert = self.db.settings.find_one_and_replace({'section':section, 'setting':setting},sboxConf,upsert=True)
        resultsUpdate = self.db.settings.update({'section': section}, {'$set': sboxConf }, upsert=True)
        return self.feeds()
        
    @cherrypy.expose
    def fetchfile(self, url=None, origin = "web-fetching", nextpage="index"):
        '''Get an url and  downloads the resorce'''
        ### Parse URL for filling metadata
        parsedURL = urlparse(url)
        print parsedURL
        
        ###  END OF PARSING
        
        response = requests.get(url)
        with self.repofs.new_file() as sample:
            for data in response.iter_content():
                sample.write(data)
        sample.filename = url
        metadata = {}
        shaXhash = self.getchksums(sample_id=sample._id)
        content_type = magic.from_buffer(self.repofs.get(sample._id).read(1024))
        content_type_mime = magic.from_buffer(self.repofs.get(sample._id).read(1024), mime=True)
        #archivedContent = self.getArchivedContent(sample_id=sample._id)
        try:
            archivedContent = self.getArchivedContent(sample_id=sample._id)
        except:
            archivedContent = None
            
        properties = {}
        
        metadata = { 'url':url, 'status': 'new', 'verdict':'unknown','origin':origin, "hits":1,
                    'content-type':content_type, 'content-type-mime':content_type_mime, 'properties':properties,
                    'sha1':shaXhash['sha1'], 'sha256':shaXhash['sha256'], 'sha512':shaXhash['sha512'],'archiveContent':archivedContent}
        
        self.db.fs.files.update({'_id': sample._id}, {'$set': metadata })
        
        ## New Mlwr analyze
        ## Send to analyze , just need to send the id of the file, send2analysis will fetch the settings
        malwareTestThread = threading.Thread(target=pooma.send2analysis, args=(sample.md5,))
        malwareTestThread.start()
        if origin == "web-cloning":
            ##- If origin is from phisher's function web cloning, we create a new phishing campaign
            ##- with prepolulated values:
            ##- template_body : content of the file is b64 encoded and copied here
            ##- creation_date: timestamp
            ##- settings_type: landing_page
            ##- original_url: url value
            landingpage_metadata = {}
            landingpage_metadata = {"landingpage_id":str(uuid.uuid4()), "creation_date":datetime.now(), "setting_type":"landingpage_template","original_url":url,"landingpage_name":"Cloned Landing Page"}
            landingpage_metadata['template_body'] = base64.standard_b64encode( self.repofs.get(sample._id).read() )
            results = self.db.settings.insert(landingpage_metadata)
            
        if nextpage == "index":
            return self.index()
        elif nextpage == "landingpages":
            return self.landingpages()
        
    @cherrypy.expose
    def tagthis(self, new_tags=None, selected_tag=None, sample_md5=None):
        '''new_tags is a list of tags enetered in the text box separated by comas
           selected_tag is a tag selected via the select widget, it is a known tag '''
        new_tag_list = []
        if selected_tag == 'None':
            pass
        else:
            new_tag_list.append(selected_tag)
        
        if len(new_tags) < 3 :
            pass
        else:    
            new_tag_list = new_tag_list + new_tags.split(',')
        print "----------------->"
        print 'New Tags: '
        print new_tag_list
        print 'Tagged Sample:' + sample_md5
        for tagged_sample in self.db.fs.files.find({'md5':sample_md5}):
            print 'Original List of Tags:'
            if 'tags' in tagged_sample:
                print tagged_sample['tags']
            else:
                print 'No Tags'
                tagged_sample['tags'] = []
        print "New list of Tags:"
        print tagged_sample['tags'] + new_tag_list
        new_list_of_tags = tagged_sample['tags'] + new_tag_list
        change_error = self.db.fs.files.update({'md5': sample_md5}, {'$set': {'tags':new_list_of_tags }})
        print change_error
        print "----------------->"
        return self.file(sampleid=sample_md5)
        
        
    @cherrypy.expose   
    def upload(self, customer_file=None, origin=None, emailFrom=None,
               emailSubject=None, emailReplyTo=None,
               emailDate=None, emailMessageID=None, emailTo=None, targetlistid = None):
        '''Receives a file from the formular and sends it to Mongo
        Sample should be less than 10Mb.
        Junksize is 8192 bytes
        10Mb is 10 490 000 bytes'''
        #with open(customer_file.filename, 'wb+') as sample: --> Write to the fs as a normal file
        #with tempfile.NamedTemporaryFile() as sample:  --> Write to a temporal file
        #- Create a file at GridFS, and write chunks of data until customer_file is over
        with self.repofs.new_file() as sample:
            while True:
                data = customer_file.file.read(8192)
                sample.write(data)
                if not data:
                    break
            sample.filename = customer_file.filename
        filecount = self.db.fs.files.find({"md5": sample.md5})
       # self.mongoLogger.info(msg.RECEIVED_FILE(origin=origin, filename=customer_file.filename, sampleID=sample.md5),
        #                         extra={'sampleID': sample.md5})
        print filecount.count()
        if filecount.count() >= 2:
            print ">> File'd MD5 hash "+sample.md5+" exists at DB. File will be deleted."
            self.repofs.delete(sample._id)
            self.db.fs.files.update_one({"md5": sample.md5 }, { "$inc" : { "hits": 1 }})
        else:
            print ">> This is a new file: "+sample.md5
            shaXhash = self.getchksums(sample_id=sample._id)
            archivedContent = self.getArchivedContent(sample_id=sample._id)
            content_type = magic.from_buffer(self.repofs.get(sample._id).read(1024))
            content_type_mime = magic.from_buffer(self.repofs.get(sample._id).read(1024), mime=True)
            properties = {}
            metadata = {'status': 'new', 'verdict':'unknown','origin':origin, "hits":1,
                    'content-type':content_type, 'content-type-mime':content_type_mime, 'properties':properties,
                    'sha1':shaXhash['sha1'], 'sha256':shaXhash['sha256'], 'sha512':shaXhash['sha512'], 'archiveContent':archivedContent}
            if origin == 'IMAP-Crawler':
                emailInfo = {}
                emailInfo['emailFrom'] = emailFrom
                emailInfo['emailSubject'] = emailSubject
                emailInfo['emailReplyTo'] = emailReplyTo
                emailInfo['emailTo'] = emailTo
                emailInfo['emailDate'] = emailDate
                emailInfo['emailMessageID'] = emailMessageID
                metadata['emailInfo'] = emailInfo
                
                
            self.db.fs.files.update({'_id': sample._id}, {'$set': metadata })
            sampleID=sample.md5
            analysisID = pooma.analysisEvent( sampleid=sampleID)
            ## New Mlwr analyze
            ## Send to analyze , just need to send the id of the file, send2analysis will fetch the settings
            ## This thread will run against AV and TE features, and will define if the file is malicious
            malwareTestThread = threading.Thread(target=pooma.send2analysis, args=(sample.md5,))
            malwareTestThread.start()
            
            ## Yara Analyzers
            ## Upload the file to Pooma's S3 bucket monitored by BinaryAlert.
            yaraThread = threading.Thread(target=pooma.yaraanalysis, args=(sample.md5,))
            yaraThread.start()
            
            ## Metadefender Analysis
            ## Ask for reputation and then send to analyze the file
            
            MetadefenderThread = threading.Thread(target=metadefender.analyze, args=(sampleID, analysisID,))
            MetadefenderThread.start()

        if origin == "phishing-targets":
            return self.editTargetList(  targetlistid = targetlistid)
        else:
            return self.index()
    
    def getArchivedContent(self, sample_id=None):
        archivedContent=[]
        try:
            with zipfile.ZipFile(self.repofs.get(sample_id)) as zipped:
                for zippedFile in zipped.infolist():
                    zfile ={'filename': zippedFile.filename,
                             'file_size': zippedFile.file_size}
                    archivedContent.append(zfile)
            return archivedContent
        except:
            return None

    
    @cherrypy.expose   
    def uploadevidence(self, customer_file=None, origin=None, md5=None):
        '''Receives a file from the formular and sends it to Mongo
        Sample should be less than 10Mb.
        Junksize is 8192 bytes
        10Mb is 10 490 000 bytes'''
        sectionSample = Template(file="webui/templates/sectionSample.html")
        #with open(customer_file.filename, 'wb+') as sample: --> Write to the fs as a normal file
        #with tempfile.NamedTemporaryFile() as sample:  --> Write to a temporal file
        #- Create a file at GridFS, and write chunks of data until customer_file is over
        with self.repofs.new_file() as evidenceFile:
            while True:
                data = customer_file.file.read(8192)
                evidenceFile.write(data)
                if not data:
                    break
            evidenceFile.filename = customer_file.filename
        content_type = magic.from_buffer(self.repofs.get(evidenceFile._id).read(1024))
        metadata = {'sample_id': md5, 'origin':origin, 'content-type':content_type}
        self.db.fs.files.update({'_id': evidenceFile._id}, {'$set': metadata })
        return self.file(sampleid=md5)
        ## --- Unti here File is created, stored at DB and its metadata updated
        ## --- We'll go back to the sample page
        # sample = self.db.fs.files.find_one({"md5":md5})
        # evidence = self.db.fs.files.find({"sample_id":md5, "origin":"sample-upload"})
        # sectionSample.sample = sample
        # sectionSample.evidence = evidence
        # self.siteIndex.menu = glossary.menu_repo
        # self.siteIndex.content = str(sectionSample)
        # return str(self.si'_id'teIndex)
    
    @cherrypy.expose
    def download(self, sample=None, fileformat='http'):
        '''Download a malware sample'''
        malwrFile = self.repofs.find_one({"md5": sample})
        if fileformat == 'http':
            return serve_fileobj(malwrFile, name=malwrFile.filename, content_type="application/x-download", disposition="attachment")
        else:
            return "How do you want to download?"

        
    @cherrypy.expose
    def downloadForensics(self, reportID=None, contenttype=None):
        '''Looks for the Forensics Report with it's given IID at MongoDB, and
        serves it as an static object for user's download.'''
        forensicsReport = self.repofs.find_one({"report_id": reportID})
        if contenttype == 'pdf':
            content = 'application/pdf'
            reportname = reportID + '.pdf'
        elif contenttype == 'xml':
            content = 'application/xml'
            reportname = reportID + '.xml'
        
        return serve_fileobj(forensicsReport, name=reportname,
                             content_type=content,
                             disposition="attachment")  
    
    
    @cherrypy.expose
    def downloadExtracted(self, extractedID=None):
        '''Fetch the extracted file'''
        extractedFile = self.repofs.find_one(
            {"scrubbed_id": extractedID})
        return serve_fileobj(extractedFile, name= extractedID + '.pdf',
                             content_type="application/pdf",
                             disposition="attachment")  
    
    
    @cherrypy.expose
    def sandbox(self, sampleID=None):
        targetFile = self.repofs.find_one({'md5': sampleID})
        sectionEmailSample = Template(file="webui/templates/sectionSandbox.html")
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionEmailSample)
        return str(self.siteIndex)
    
    
    @cherrypy.expose
    def emailSample(self, sampleID=None):
        attachment = self.repofs.find_one({'md5': sampleID})
        print dir(attachment)
        
        sectionEmailSample = Template(file="webui/templates/sectionEmailSample.html")
        sectionEmailSample.attachment = attachment
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionEmailSample)
        return str(self.siteIndex)
    
#######         PHISHING ENDPOINTS
    
    @cherrypy.expose
    def phishing(self, phishingid=None):
        has_landingpage = 0
        phishingCampaign = Phishing( phishingid = phishingid)
        ## Get all phishing campaigns to poblate the select option for loading campaigns
        allCampaigns = self.db.settings.find({"setting_type": "phishing_campaign"})
         ## Get all phishing campaigns to poblate the select option for loading campaigns
        allLandingPages = self.db.settings.find({"setting_type": "landingpage_template"})
        
        ## Get last campaign to be showed
        ## This is using phishing class
        lastCampaign = phishingCampaign.loadCampaignSettings()
        
        #lastCampaign = self.db.settings.find({"setting_type": "phishing_campaign"})
        ## Get the details of the target email list
        targetlist = self.db.settings.find_one({"setting_type": "phishing-target-list", "targetlist_id": lastCampaign['target_email_list']})
        ### Get all the available templates
        templates = self.db.settings.find({"setting_type": "phishing_template"})
        ## - Get all running events
        runningevents = self.db.settings.find({"setting_type": "campaign_run", "state": "running", "campaign_id" :lastCampaign['campaign_id']})
        ### Get all the customized LANDING PAGES that are associated with this campaign
        
        landingpage = phishingCampaign.loadCustomizedLandingPages(campaign = lastCampaign['campaign_id'])
        customizedLandingPages = []
        if landingpage is None:
            has_landingpage = 0
            customizedLandingPages = []
        else:
            has_landingpage = 1
            customizedLandingPages.append(landingpage)
            
        widgetCustomLandingPages = Template(file="foundation/widgetLandingPage.html")
        widgetCustomLandingPages.templates_list = customizedLandingPages
        print has_landingpage
        ## Only one campaing is
        
        ## Load the last campaing that was used
        #phishingCampaign = Phishing( phishingid )
        
        ##- Build Webpage
        widgetTemplates = Template(file="foundation/widgetPhishingTemplates.html")
        widgetTemplates.templates_list = lastCampaign['phishing_templates']
        
        campaign = Template(file="foundation/sectionCampaigns.html")   
        campaign.lastCampaign = lastCampaign
        campaign.targetlist = targetlist
        campaign.has_landingpage = has_landingpage
        campaign.allCampaigns = allCampaigns
        campaign.allLandingPages  = allLandingPages
        campaign.widgetCustomLandingPages = str(widgetCustomLandingPages)
        campaign.phishingTemplates = templates
        campaign.runningEvents = runningevents
        campaign.widgetPhishingTemplates = str(widgetTemplates)

        phishers = Template(file="foundation/index.html")        
        phishers.main_section = campaign
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def phishing_logs(self, campaignid = None, runid = None):
        #Get Logs
        run_logs = self.db.phishinglogs.find({"log_type": "campaign_run" , "campaign_id" : campaignid, "run_id": runid }).sort([("timestamp", pymongo.ASCENDING)])
        ##- Build Webpage
        logsTable = Template(file="foundation/sectionPhishingLogs.html")
        logsTable.run_logs = run_logs
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = logsTable
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def setAsLastUsedCampaign(self, newcampaign = None):
        phishingCampaign = Phishing(phishingid = newcampaign)
        phishingCampaign.setAsLastUsed()
        return self.phishing()
    
    @cherrypy.expose
    def savePhishingCampaignSettings(self, campaignid = None, campaign_name = None, campaign_description = None,
                                     delay_between_emails = None, target_email_list = None):
        phishingCampaign = Phishing(phishingid = campaignid)
        ## since campaign_id is None, save Settings will create a new campaign
        if campaignid is None:
            newCampaign = phishingCampaign.saveSettings()
            phishingCampaign.setAsLastUsed()
            print "(DBG savePhishingCampaignSettings) " + str(newCampaign)
        else:
            settings = {}
            settings['campaign_name'] = campaign_name
            settings['campaign_description'] = campaign_description
            settings['delay_between_emails'] = delay_between_emails
            settings['target_email_list'] = target_email_list
            phishingCampaign.saveSettings(campaign_settings = settings)
        return self.phishing()
    
    
    @cherrypy.expose
    def savePhishingTemplateSettings(self, campaign_id = None, template_id = None):
        '''This function will create a new template as a master or one attached to a campaign'''
        phishingCampaign = Phishing( phishingid = campaign_id, template_id = template_id )
        newSettings = phishingCampaign.createNewTemplate()
        print "(DBG savePhishingTemplateSettings) " + str(newSettings)
        #phishingCampaign = Phishing(phishingid = campaign_id)
        ## since campaign_id is None, save Settings will create a new campaign
        #newCampaign = phishingCampaign.saveSettings()
        #phishingCampaign.setAsLastUsed()
        #print newCampaign
        if campaign_id == None:
            return self.phishingtemplates()
        else:
            return self.phishing()
    
    
    @cherrypy.expose
    def addCustomizedLandingPage(self, template=None, campaign=None):
        
        ## Set the order of this 
        ##  Look for the template to clone it
        template = self.db.settings.find_one({"landingpage_id": template})
        template['landingpage_id'] = str(uuid.uuid4())
        template['timestamp'] = datetime.now()
        template['setting_type'] = "landingpage_template_customized"
        template['campaign_id'] = campaign
        if '_id' in template: del template['_id']
        self.db.settings.insert(template)
        return self.phishing()
    
    @cherrypy.expose
    def saveTemplateSettings(self, template_id = None, template_name = None, content_type = None, shortener = None,
                             email_service = None, template_subject = None, template_body = None):
        template = self.db.settings.find_one({"template_id": template_id})
        template['template_name'] = template_name
        template['email_service'] = email_service
        template['template_subject'] = base64.standard_b64encode( template_subject )
        template['template_body'] = base64.standard_b64encode( template_body.encode('utf-8') )
        template["shorten_links"] =  shortener
        results = self.db.settings.update_one( {'template_id':template_id },
                                               {'$set': template }, upsert = True  )
        return self.editPhishingTemplate( templateid = template_id )
    
    @cherrypy.expose
    def saveLandingPageSettings(self, landingpage_id = None, landingpage_name = None, original_url = None, template_body = None):
        template = self.db.settings.find_one({"landingpage_id": landingpage_id})
        template['landingpage_name'] = landingpage_name
        template['template_body'] = base64.standard_b64encode( template_body.encode('utf-8') )
        template["original_url"] =  original_url
        results = self.db.settings.update_one( {'landingpage_id':landingpage_id },
                                               {'$set': template }, upsert = True  )
        return self.editLandingPage( templateid = landingpage_id )
    
    @cherrypy.expose
    def deleteTemplate(self, templateid = None):
        results = self.db.settings.delete_one( {'template_id':templateid })
        return self.phishing()
        #if return_to == 'campaign':
        #    return self.phishing()
        #elif return_to == 'template':
        #    return self.editPhishingTemplate( templateid = template_id )
    @cherrypy.expose
    def deleteItem(self, itemid = None):
        results = self.db.settings.delete_one( {'landingpage_id':itemid })
        return self.landingpages()
        
    @cherrypy.expose
    def deleteCampaign(self, phishing_id = None):
        '''Remove a phishing campaign document and all customized templates wich are
        part of this campaign.
        '''
        phishingCampaign = Phishing(phishingid = phishing_id)
        phishingCampaign.deleteCampaign()
        return self.phishing()
    
    @cherrypy.expose
    def editCampaign(self, phishing_id = None):
        phishing = self.db.settings.find_one({'campaign_id': phishing_id})
        targets = self.db.settings.find({"setting_type": "phishing-target-list"})
         ##- Build Webpage
        main = Template(file="foundation/sectionEditPhishingCampaign.html")   
        main.phishing = phishing
        main.targets = targets

        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    
    @cherrypy.expose
    def saveTargetSettings(self, targetlist_id = None, targetlist_name = None, csvfile = None):
        settings={}
        sample = self.db.fs.files.find_one({'md5': csvfile })
        csvfile = pd.read_csv(self.repofs.get(sample["_id"]))
        ##self.siteIndex.content = str(csvfile.to_html())
        #settings = self.db.settings.find_one({"targetlist_id": targetlist_id})
        #print csvfile
        loop = len(csvfile['username']) - 1
        targets = []
        while loop >= 0:
            target = {"name": csvfile['username'][loop], "email": csvfile['email'][loop]}
            targets.append(target)
            loop = loop - 1
        settings['targetlist_name'] = targetlist_name
        settings['targets'] = targets
        print settings
        results = self.db.settings.update_one( {'targetlist_id':targetlist_id },
                                              {'$set': settings }, upsert = True  )
        return self.phishingtargets()
        #return self.editTargetList( targetlistid = targetlist_id )
        
    @cherrypy.expose
    def addTargetList(self):
        phishingCampaign = Phishing()
        phishingCampaign.createNewTargetList()
        return self.phishingtargets()
    
    @cherrypy.expose
    def deleteTargetList(self, targetlistid = None):
        phishingCampaign = Phishing()
        phishingCampaign.deleteTargetList(targetlist_id = targetlistid)
        return self.phishingtargets()
    
    
    @cherrypy.expose
    def phishingtargets(self):
        targets = self.db.settings.find({"setting_type": "phishing-target-list"})
        ##- Build Webpage
        main = Template(file="foundation/sectionPhishingTargets.html")   
        main.targets_list = targets
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def viewtargets(self, targetlistid):
        targets = self.db.settings.find_one({"targetlist_id": targetlistid,"setting_type": "phishing-target-list"})
        ##- Build Webpage
        main = Template(file="foundation/widgetViewTargets.html")   
        main.targets = targets
        #phishers = Template(file="foundation/index.html")        
        #phishers.main_section = main
        ##- End of page building
        return str(main)
    
    @cherrypy.expose
    def phishingtemplates(self):
        templates = self.db.settings.find({"setting_type": "phishing_template"})
        ##- Build Webpage
        widgetTemplates = Template(file="foundation/widgetPhishingTemplates.html")
        widgetTemplates.templates_list = templates
        
        main = Template(file="foundation/sectionPhishingTemplates.html")   
        main.widgetPhishingTemplates = str(widgetTemplates)

        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def landingpages(self):
        templates = self.db.settings.find({"setting_type": "landingpage_template"})
        ##- Build Webpage
        widgetTemplates = Template(file="foundation/widgetLandingPage.html")
        widgetTemplates.templates_list = templates
        
        main = Template(file="foundation/sectionLandingPages.html")   
        main.widgetPhishingTemplates = str(widgetTemplates)

        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def runPhishingCampaign(self, campaignid = None):
        campaign = Phishing( phishingid = campaignid)
        phishingThread = threading.Thread(target=campaign.runCampaign)
        phishingThread.start()
        ##campaign.runCampaign()
        return self.phishing()
    
    @cherrypy.expose
    def editPhishingTemplate(self, templateid = None):
        ##- Get settings for this template
        template = self.db.settings.find_one({"template_id" : templateid})
        ##- Get the list of all available email services
        emailServices = self.db.settings.find({"setting_type": "phishing_email_service"})
        ##- Build Webpage
        main = Template(file="foundation/sectionEditPhishingTemplate.html")   
        main.template = template
        main.emailServices = emailServices
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def editLandingPage(self, templateid = None):
        template = self.db.settings.find_one({"landingpage_id" : templateid})
        ##- Build Webpage
        main = Template(file="foundation/sectionEditLandingPage.html")   
        main.template = template
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def editTargetList(self, targetlistid = None):
        targetlist = self.db.settings.find_one({"targetlist_id" : targetlistid})
        csvfiles = self.db.fs.files.find({'origin':'phishing-targets'})
        ##- Build Webpage
        main = Template(file="foundation/sectionEditTargetList.html")
        main.csvfiles = csvfiles
        main.list = targetlist
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)
    
    @cherrypy.expose
    def phishingpreview(self, templateid = None):
        template = self.db.settings.find_one({"template_id":templateid})
        sectionPhishingtTemplate = Template(template["template_body"])
        return base64.standard_b64decode(str(sectionPhishingtTemplate))
    
    @cherrypy.expose
    def emailservices(self):
        emailservices = self.db.settings.find({"setting_type": "phishing_email_service"})
        ##- Build Webpage
        main = Template(file="foundation/sectionEmailServices.html")   
        main.emailservices = emailservices
        phishers = Template(file="foundation/index.html")        
        phishers.main_section = main
        ##- End of page building
        return str(phishers)

    
    @cherrypy.expose
    def sendEmail(self, sampleid= None, efrom_domain=None, send_to=None, subject=None, template=None,
                  as_url=None, with_shortener=None):
        textTemplates = ('001','003')
        htmlTemplates = ('002','004')
        
        efrom_user="pooma"
        efrom = efrom_user + '@' + efrom_domain
        sampleFile = self.db.fs.files.find_one({'md5': sampleid})
        
        #Enclosing outer Message
        email = MIMEMultipart()
        email['Subject'] = subject 
        email['From'] = efrom
        #email['Date'] = formatdate(localtime=True)
        email['To'] = send_to

        ## Set attachment
        #msg.attach(MIMEText(message))
        msg = MIMEBase('application','zip')

        with self.repofs.get(sampleFile['_id']) as attachment:
            #tempZip.write(samplefile, compress_type = zipfile.ZIP_DEFLATED)
            while True:
                data =  attachment.read(2**20)
                if not data:
                    #attachment.read('abcdefg')
                    break
                msg.set_payload(data)
        
        
        # Set email message
        # Turn these into plain/html MIMEText objects
        if template in textTemplates:
            templateFile = "phishing_templates/" + str(template) + ".txt"
            phishingTemplate = Template(file=templateFile)
            phishingBody = MIMEText(str(phishingTemplate), "plain" )
        elif template in htmlTemplates:
            ### Get random phishing
            with open("feeds/urls", "r") as feeds:
                urlList = feeds.readlines()
                phishingURL = urlList[ random.randint(1, len(urlList)) ]
                print "PhishingURL: " + phishingURL
                templateFile = "phishing_templates/" + str(template) + ".html"
                phishingTemplate = Template(file=templateFile)
                phishingTemplate.url = phishingURL
                print str(phishingTemplate)
                phishingBody = MIMEText(str(phishingTemplate), "html" )
            
        ##part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        ##msg.attach(part1)
        ##message.attach(part2)
        
                
        encoders.encode_base64(msg)
        msg.add_header('Content-Disposition', 'attachment', filename=attachment.filename)
        email.attach(msg)
        email.attach(phishingBody)
        server = smtplib.SMTP_SSL(config.SMTP_SERVER)
        server.login(config.SMTP_USER, config.SMTP_PASSWD)
        server.sendmail(efrom, send_to, email.as_string())
        server.close()
        return self.file(sampleid=sampleid)
    
    
    def saveItemSettings(self, settingid=None, settingtype=None, landingpage=None):
        '''This section aim to be an universal endpoint for saving settings on all platform
        settingid - is the uuid of any settings document
        settingtype - is the type of settings e.g phishing_template
        landingpage - the name of the internal page where the save item function will return
                    after doing the changes
        '''
        return 0
    
######   LANDING PAGE ENDPOINTS   ######    
    @cherrypy.expose
    def landingpage(self, lpid = None):
        index = Template('''$landingpage''')
        template = self.db.settings.find_one({"landingpage_id":lpid})
        index.landingpage = Template(base64.standard_b64decode(template["template_body"]))
        return str(index)
    
    @cherrypy.expose
    def submitdata(self, tab = None, userLoginId = None, password = None, campaignid = None, emulated_service='o365',
                   rememberMe=None,flow=None,mode=None,action=None,withFields=None, authURL=None, nextPage=None,
                   showPassword=None,countryCode=None,countryIsoCode="MX"):
        
        credentials = self.db.credentials.insert({'userid': uuid.uuid4(),
                                                  'campaignid': campaignid,
                                                  'user': userLoginId,
                                                  'password':password,
                                                  'timestamp':datetime.now(),
                                                  'emulated_service':emulated_service})
        print "*** !!! Credentials from user: "
        print userLoginId, password, nextPage
        if nextPage == 'thiswasatest':
            return self.thiswasatest()
        else:
            return 0
        
    @cherrypy.expose   
    def thiswasatest(self):
        main = Template('''This was a Phishing Test''')
        return str(main)
        
    @cherrypy.expose
    def editor(self, itemid=None):
        template = self.db.settings.find_one({"landingpage_id":itemid})
        aceeditor = Template(file="foundation/ace_editor.html")
        aceeditor.template = base64.standard_b64decode(template["template_body"])
        return str(aceeditor)
        
#####       End Of Phishing Endpoints        
        
    @cherrypy.expose
    def reName(self, sampleID=None):
        sampleFile = self.db.fs.files.find_one({'md5': sampleID})
        sectionRename = Template(file="webui/templates/sectionRename.html")
        sectionRename.sampleid = sampleID
        sectionRename.sampleFile = sampleFile
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionRename)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def archiveContent(self, sampleID=None):
        sampleFile = self.db.fs.files.find_one({'md5': sampleID})
        sectionArchiveContent = Template(file="webui/templates/sectionArchiveContent.html")
        sectionArchiveContent.sampleid = sampleID
        try:
            sectionArchiveContent.archiveContent = sampleFile['archiveContent']
        except KeyError:
            sectionArchiveContent.archiveContent = None
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionArchiveContent)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def doRename(self, sampleID=None, newname=None):
        sampleFile = self.db.fs.files.find_one({'md5': sampleID})
        print sampleFile
        print sampleFile['filename']
        print newname
        self.db.fs.files.update({'_id': sampleFile['_id']}, { '$set' : {'filename': newname }})
        return self.file(sampleid=sampleID)
    
    @cherrypy.expose
    def archive(self, sampleID=None):
        sectionArchive = Template(file="webui/templates/sectionArchive.html")
        sectionArchive.sampleid = sampleID
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionArchive)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def doArchive(self, sampleid=None, method=None, filename=None, encpassword=None):
        print 'ID: ' + sampleid
        print 'M: ' + method
        print 'F: ' + filename
        print 'E: ' + encpassword
        sampleFile = self.db.fs.files.find_one({'md5': sampleid})
        defaultTempStore = '/var/www/pooma/'
        # 01. read file from GridFS and store it on temp_file
        #with self.repofs.get(sampleFile['_id']) as filesample:
         
        temp4Zip  = tempfile.NamedTemporaryFile(delete=False, prefix="PooMAZipTmp-")
        with self.repofs.get(sampleFile['_id']) as samplefile:
            #tempZip.write(samplefile, compress_type = zipfile.ZIP_DEFLATED)
            while True:
                data =  samplefile.read(2**20)
                if not data:
                    break
                temp4Zip.write(data)
                
        temp4Zip.close()
            
        # 02. Compress the file
        print temp4Zip.name
        
        with zipfile.ZipFile(temp4Zip.name + '.ZIP','w') as zf:
            zf.write(temp4Zip.name, compress_type = zipfile.ZIP_DEFLATED)

        
        # 03 Insert the file to DB
        with self.repofs.new_file() as archiveFile:
            with open(temp4Zip.name + '.ZIP', 'r') as zipFile:
                while True:
                    data = zipFile.read(2**20)
                    if not data:
                        break
                    archiveFile.write(data)
        if filename == None:
            archiveFile.filename = zf.filename
        else:
            archiveFile.filename = filename
        
        content_type = magic.from_buffer(self.repofs.get(archiveFile._id).read(1024))
        content_type_mime = magic.from_buffer(self.repofs.get(archiveFile._id).read(1024), mime=True)
        metadata = {'sample_id': sampleid, 'origin': 'archived' , 'content-type':content_type, 'content-type-mime':content_type_mime }
        self.db.fs.files.update({'_id': archiveFile._id}, {'$set': metadata })
        print sampleFile['_id']
        return self.file(sampleid=sampleid)
    
    
    @cherrypy.expose
    def reAnalyze(self, sampleID=None):
        '''re analyze a file that is already in the filesystem'''
        # Create an Anaylsis Event
        analysisID = pooma.analysisEvent( sampleid=sampleID)
        print analysisID
        #DebugMetadata = {}
        logExtraMetadata = {}
        #Define wich service PooMA will star a Thread
        default_policy = self.db.settings.find_one({"setting_type":"file_analysis_policy", "default_policy": True})
        #DebugMetadata = {'sampleID': sampleID, 'feature': 'Debug', 'analysisID': analysisID}
        #log_message = "Active Policy: " + str(default_policy)
        #self.mongoLogger.info(log_message ,extra=logExtraMetadata)
        ## Send to analyze , just need to send the id of the file, send2analysis will fetch the settings
        ## All Threads receive md5sum, each thread will lookup for details of the file as the Mongo ._id
        logExtraMetadata = {'sampleID': sampleID, 'feature': 'Core', 'analysisID': analysisID}
        
        # Is sandBlast enabled?
        if 'sandblast_policy' in default_policy:
            log_message = "Analysis at Check Point SandBlast is ENABLED, I'll start analysis thread."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)
            
            malwareTestThread = threading.Thread(target=pooma.send2analysis, args=(sampleID,))
            malwareTestThread.start()
            
        else:
            log_message = "Analysis at Check Point SandBlast is DISABLED."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)  
        
        ## Is Binary Alert Enabled?
        if 'binaryalert_policy' in default_policy:
            log_message = "Analysis at Binary Alert with Yara signatures is ENABLED, I'll start analysis thread."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)
            
            yaraThread = threading.Thread(target=pooma.yaraanalysis, args=(sampleID,))
            yaraThread.start()
           
        else:
            log_message = "Analysis at Binary Alert with Yara signatures is DISABLED."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)
        
        ## Is Metadefender Enabled?
        if 'metadefender_policy' in default_policy:
            log_message = "Analysis at Metadefender is ENABLED, I'll start analysis thread."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)
            
            
            MetadefenderThread = threading.Thread(target=metadefender.analyze, args=(sampleID, analysisID,))
            MetadefenderThread.start()
            
            #MetadefenderThread = threading.Thread(target=pooma.metadefender, args=(sampleID, analysisID,))
            #MetadefenderThread.start()
           
        else:
            log_message = "Analysis at Metadefender is DISABLED."
            self.mongoLogger.info(log_message ,extra=logExtraMetadata)
      
        return self.file(sampleid=sampleID)
    
    @cherrypy.expose
    def metadefender(self, sampleID=None):
        metadefenderMetadata = self.db.analysis.find_one({'malwr_md5': sampleID})
        sectionMetadefender = Template(file="webui/templates/sectionMetadefender.html")
        #sectionMetadefender.sampleid = sampleID
        sectionMetadefender.metadefenderMetadata = metadefenderMetadata
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionMetadefender)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def ipurlanalysis(self, urlhash=None):
        analysis = self.db.analysis.find_one({'url_id': urlhash})
        sectionIPURLAnalysis = Template(file="webui/templates/sectionIPURLAnalysis.html")
        sectionIPURLAnalysis.analysis = analysis
        self.siteIndex.menu = glossary.menu_repo
        self.siteIndex.content = str(sectionIPURLAnalysis)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def crawlList(self, customer_file=None):
        ##customer_file.file.read(8192)
        urlCrawlerThread = threading.Thread(target=pooma.urlcrawler, args=(customer_file,))
        urlCrawlerThread.start()
            
    @cherrypy.expose
    def hashlookup(self, hashsum=None):
        #Does this HASH exist on the local cache?
        # Local cache means:
        # Query for MD5 in fs.files
        # Query for MD5 in hashlist
        # ==========================================
        # Does this hash exist as an analized file?
        queryResult = self.db.fs.files.find_one({'md5': hashsum})
        print queryResult
        #Does this hash exist as a analyzed URL
        #Insert hash metadata into hashes DB
        insertLookUp = self.db.hashlist.insert({'sampleHash': hashsum,
                                                'algo': 'md5',
                                                'source': 'webui_lookup'})
       #Perform lookup against Metadefender
        metadefenderMetadata = pooma.metadefender(hashsum=hashsum)
        return self.metadefender(sampleID = hashsum)
    
    @cherrypy.expose
    def ipurllookup(self, ipurl=None, origin=None):
        #Phishtank only has domains
        urlhash = hashlib.md5()
        urlhash.update(ipurl.encode('utf-8'))
        url_id = urlhash.hexdigest()
        insertLookUp = self.db.ipurl.insert({'ipurl': ipurl,
                                             'url_id': url_id,
                                                'source': origin,
                                                'verdict': 'unknown'})
       #Perform lookup against Metadefender
        phishtankMetadata = pooma.phishtank(url=ipurl, url_id=url_id)
        print phishtankMetadata
        return self.ipurlanalysis(urlhash=url_id)
        #self.metadefender(sampleID = hashsum)
    
    
    
    @cherrypy.expose
    def deleteSample(self, sampleID=None):
        '''Delete a file and all its related content @ GridFS'''
        ## Delete Emulatios Reports
        reportsForDelete = self.repofs.find({"malwr_md5": sampleID})
        
        for report in reportsForDelete:
            print "Delete Report: " + str(report._id) + " Filename: " + str(report.filename)
            self.repofs.delete(report._id)
    
        ## Delete Scrubbed Files
        scrubbedFiles = self.repofs.find({"file_md5": sampleID})
        
        for scrubbed in scrubbedFiles:
            print "Delete ScrubbedFile: " + str(scrubbed._id) + " Filename: " + str(scrubbed.filename)
            self.repofs.delete(scrubbed._id)
            
        # Delete Main File
        fileForDelete = self.repofs.find_one({"md5": sampleID})
        #print fileForDelete._id
        self.repofs.delete(fileForDelete._id)
        
        
        return self.index()
    
    @cherrypy.expose
    def deleteEmulationReport(self, reportid=None):
        '''Delete a file and all its related content @ GridFS'''
        #This should also delete foresnsics reports asociated
        fileForDelete = self.repofs.find_one({"report_id": reportid})
        #print fileForDelete._id
       # print fileForDelete.malwr_md5
        self.repofs.delete(fileForDelete._id)
        return self.file(sampleid=fileForDelete.malwr_md5)
    
    @cherrypy.expose
    def deleteArchive(self, sampleID=None, archiveID=None):
        '''Delete a file and all its related content @ GridFS'''
        #This should also delete foresnsics reports asociated
        fileForDelete = self.repofs.find_one({"md5": archiveID})
        #print fileForDelete._id
        self.repofs.delete(fileForDelete._id)
        return self.file(sampleid=sampleID)
    
    @cherrypy.expose
    def oneclickcli(self,cmd = None):
        sectionCmdsOutput = Template(file="webui/templates/sectionCmdsOutput.html")
        sectionCmdsOutput.command = cmd
        result_find = self.db.settings.find({'setting_type':'node', 'feed_name':'sandblast'})
        sandblast_pvc_nodes = []
        for node in result_find:
            sandblast_pvc_nodes.append(node)
        sectionCmdsOutput.nodes = sandblast_pvc_nodes
        active_node = self.db.settings.find_one({'section':'1clickcli'})
        sectionCmdsOutput.oneclick_settings = active_node
        if cmd == None:
            sectionCmdsOutput.cmdoutput = 'No CMD output'
        else:
#te100x = '172.20.22.70'
#ngfw2200 = '172.20.22.220'
            hosts = active_node['user'] + '@' + active_node['ipv4']
            #hosts = ['admin@sbgw']
            #hosts = 'admin@192.168.1.200'
#env.hosts = [te100x,ngfw2200]
            #hosts.append(sbgw)
            sectionCmdsOutput.cmdoutput = sandblast_fabric.get_cmds(cmd = cmd, hosts = hosts, passwd = node['passwd'])
            
        self.siteIndex.menu = glossary.menu_tools
        self.siteIndex.content = str(sectionCmdsOutput)
        return str(self.siteIndex)
    
    @cherrypy.expose
    def set1clkNode(self, node_hostname=None):
        '''Save Active Node'''
        ## SANDBLAST SETTINGS
        result_find = self.db.settings.find_one({'setting_type':'node', 'hostname':node_hostname})
        print result_find
        sboxConf= {
                "2FA": True,    
                "LogAllQueries": True,
                "active_node": node_hostname,
                "ipv4": result_find['ipv4_addr'],
                "passwd": result_find['passwd'],
                "section": "1clickcli",
                "user": result_find['user']
                }      
        result_insert = self.db.settings.find_one_and_replace({'section':'1clickcli' },sboxConf,upsert=True)
        return self.oneclickcli()
    
    
    
if __name__ == '__main__':
    current_dir = os.path.dirname(os.path.abspath(__file__))
    cherrypy.config.update({'environment': 'production',    
                            'log.screen': True,
                            'log.error_file': 'pooma.log',
                            'server.socket_host': config.poomaAppIP,
                            'server.socket_port': config.poomaAppPort})

    conf = {'/images' : {'tools.staticdir.on': True,
                         'tools.staticdir.dir': os.path.join(current_dir,'webui/images'),
                         'tools.staticdir.content_types':{'jpg':'image/jpeg',
                                                          'png':'image/png',
                                                          'gif':'image/gif'}},
            '/favicon.ico': {'tools.staticfile.on': True,
                             'tools.staticfile.filename': os.path.join(current_dir ,'webui','favicon.ico')},
            
            '/forensics' : {'tools.staticdir.on': True,
                            'tools.staticdir.dir': os.path.join(current_dir,'forensics'),
                            'tools.staticdir.content_types':{'jpg':'image/jpeg',
                                                            'png':'image/png',
                                                            'gif':'image/gif',
                                                            'js':'application/json',
                                                            'js':'application/javascript',
                                                            'html':'text/html',
                                                            'css':'text/css'}},

            '/novnc' : {'tools.staticdir.on': True,
                            'tools.staticdir.dir': os.path.join(current_dir,'noVNC'),
                            'tools.staticdir.content_types':{'jpg':'image/jpeg',
                                                            'png':'image/png',
                                                            'gif':'image/gif',
                                                            'js':'application/json',
                                                            'js':'application/javascript',
                                                            'html':'text/html',
                                                            'css':'text/css'}},

            '/Style.css' : {'tools.staticfile.on': True, 
                            'tools.staticfile.filename': os.path.join(current_dir,'webui/Style.css')},
                            
            '/css' : {'tools.staticdir.on': True, 
                      'tools.staticdir.dir': os.path.join(current_dir,'foundation/css')},
             '/ace-builds' : {'tools.staticdir.on': True, 
                      'tools.staticdir.dir': os.path.join(current_dir,'ace-builds')}}
    root = Root()
    #root.sample = Sample()
    
    cherrypy.quickstart(root, '/', config=conf)
