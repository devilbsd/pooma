from fabric.api import *
from fabric.colors import *
from fabric.tasks import *
import fabric.contrib.files
import re
import config

#publicIP = config.poomaFrontIP
publicIP = '10.0.0.150:6060' 
#env.hosts=[]

@task
def get_cmds(cmd = None, hosts = None, passwd = None):
	print hosts
	env.host_string = hosts
	#env.hosts.append(hosts)
	#print env.host
	#env.user = 'admin'	
	env.password = passwd
	with settings(warn_only=True):
		try:
			result = run(cmd)
		except NetworkError:
			return 'Failed to connect to host: ' + hosts
		if result.failed:
			return 'There was an error executing the command'
		else:
			dnsbg = re.compile('bg')
			return result
	# output = []
	# NGFW_ONLY_CLI = ['tecli s s']
	# TE_ONLY_CLI = ['tecli s e v s'] + NGFW_ONLY_CLI
	# for cmd in TE_ONLY_CLI:
	# 	output.append(run(cmd))
	# return output
	
@task
def setHAProxyConfig( host = None):
	env.host_string = host
	#env.hosts.append(hosts)
	#print env.host
	env.user = 'admin'	
	env.password = 'x3w9db7tf54'
	with settings(warn_only=True):
		try:
			result = run('''sudo curl "http://''' + publicIP + '''/haproxy_cfg" > /tmp/haproxy.cfg''')
			result = run('''sudo mv /tmp/haproxy.cfg /etc/haproxy/''')
			result = run('''sudo service haproxy reload''')
		except NetworkError:
			return 'Failed to connect to host: ' + hosts
		if result.failed:
			return 'There was an error applying the configuration'
			return result
		
		
@task
def addSandBlast( host = None, password = None):
	env.host_string = host
	#env.hosts.append(hosts)
	#print env.host
	#env.user = 'admin'	
	env.password = password
	with settings(warn_only=True):
		try:
			hostname = run('hostname')
			system = run('fw ver')
		except NetworkError:
			return 'Failed to connect to host: ' + host
		#if result.failed:
		#	return 'There was an error applying the configuration'	
	return hostname, system		

