import config
import messages
import messages as msg
import mimetypes
import requests
import json
from pymongo import MongoClient
import gridfs
import logging
from log4mongo.handlers import MongoHandler
import threading
import time
import uuid
import base64
from ftplib import FTP

client = MongoClient(config.mongodburi)
db = client.pooma
repofs = gridfs.GridFS(db)

mongoLogger = logging.getLogger('PooMALog')
mongoHandler = MongoHandler(host=config.mongodburi,
                            database_name='pooma',
                            collection='logs')
mongoLogger.addHandler(mongoHandler)

def loadSettings( crawlerid = None ):
        return db.settings.find_one({"crawlerid": crawlerid})


def test_ftp_connection(crawler = None):
        '''This function will connect to the FTP website and test login, the it returns the
        message returned byt the Server'''
        settings = loadSettings(crawlerid = crawler)
        print settings['hostname']
        try:
                ## This will try the connectio with the server
                ftp = FTP(host = settings['hostname'])
                message = ftp.login(user=settings['user'], passwd = settings['passwd'])
                ftp.close()
                print message
        except:
                message = 'Error when connecting'
        return message