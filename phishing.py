import config
import messages
import messages as msg
import phishing_messages as pmsg
import mimetypes
import requests
import json
from pymongo import MongoClient
import gridfs
import logging
from log4mongo.handlers import MongoHandler
import threading
import time
import uuid
import base64
from datetime import datetime
import smtplib
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import random
from Cheetah.Template import Template
import threading

class Phishing:
        '''This class will create a phishing campaign to be test with internal users.
        Phishing campaigns are composed a follows:
                * Phishing camplaign
                        + campaign settings
                                - Number of tries
                                - Use of shorteners
                                - Choose the origin email accounts
                        + phishing templates
                                - templates can have different capabilities like urls or
                                attachments.
                                - 5 templates per campaign
                                - all of them takes campaing settings if
                                if the template allows
        '''
        def __init__(self, phishingid = None, template_id = None):
                #Database connections
                #Settings Database
                self.client = MongoClient(config.mongodburi)
                self.db = self.client.pooma
                self.repofs = gridfs.GridFS(self.db)
                #Logging collection in mongodb
                self.mongoLogger = logging.getLogger('PhishingLog')
                if not self.mongoLogger.handlers:
                        self.mongoHandler = MongoHandler(host=config.mongodburi,     
                                    database_name='pooma',
                                    collection='phishinglogs')
                        self.mongoLogger.addHandler(self.mongoHandler)
                ## Objects value
                self.phishingCampaignID = phishingid
                self.phishingTemplateID = template_id
                self.objectQuery = {"setting_type":"phishing_campaign",
                                        "campaign_id": self.phishingCampaignID}
                
                
                
                print "(DBG phishing.__init__) " + str(self.objectQuery)
                
        def setAsLastUsed(self):
                last_used_on = {"last_used":"yes"}
                last_used_off = {"last_used":"no"}
                ### Look who is the last used actually
                results = self.db.settings.find_one( {"setting_type":"phishing_campaign","last_used":"yes"})
                print "### Inside Class"
                for x in results:
                        print x
                ### Update last used to no, on the actual last used campaign
                results = self.db.settings.update_one( {"setting_type":"phishing_campaign","campaign_id":results["campaign_id"] },
                                                        {"$set": last_used_off } )
                ##Update desired campaing to be active
                results = self.db.settings.update_one( {"setting_type":"phishing_campaign","campaign_id": self.phishingCampaignID },
                                                        {"$set": last_used_on } )
                return 0
        
        def createCampaignSettings(self):
                ##Creates a new campaign as a dictionary, inserts into db and returns the object description
                self.newCampaign = {}
                self.newCampaign['creation_date'] = datetime.now()
                self.newCampaign['last_change'] = datetime.now()
                self.newCampaign['campaign_name'] = 'New campaign'
                self.newCampaign['campaign_description'] = 'This is a new campaign'
                self.newCampaign['expiration_date'] = 'xxxx'
                self.newCampaign['target_email_list'] = "0dc119cd-c48f-48ae-a92e-18dd483c125f"
                self.newCampaign['delay_between_emails'] = 10
                self.newCampaign['status'] = 'Initialized'
                self.newCampaign['last_used'] = 'yes'
                self.newCampaign['setting_type'] = 'phishing_campaign'
                self.newCampaign['campaign_id'] = str(uuid.uuid4())
                return self.newCampaign
        
        def createNewTargetList(self):
                self.newSettings = {}
                self.newSettings['setting_type'] = 'phishing-target-list'
                self.newSettings['targetlist_name'] = 'New TargetList'
                self.newSettings['targetlist_id'] = str(uuid.uuid4())
                self.newSettings['targets'] = []
                self.db.settings.insert(self.newSettings)
                
        def deleteTargetList(self, targetlist_id = None):
                query = {"setting_type":"phishing-target-list", "targetlist_id": targetlist_id }
                self.db.settings.remove(query)
                
        def deleteCampaign(self):
                results = self.db.settings.delete_one( {'campaign_id': self.phishingCampaignID })
                results = self.db.settings.delete_many( {'parent_campaign': self.phishingCampaignID })
                ### Set a new active campaign
                query = {"setting_type":"phishing_campaign" }
                campaign = self.db.settings.find_one(query)
                last_used_on = {"last_used":"yes"}
                results = self.db.settings.update_one( {"setting_type":"phishing_campaign","campaign_id": campaign['campaign_id'] },
                                                        {"$set": last_used_on } )
                
        
                
        def createTemplateSettings(self):
                ##Creates a new campaign as a dictionary, inserts into db and returns the object description
                self.newSettings = {}
                if self.phishingCampaignID == None:
                        self.newSettings['setting_type'] = 'phishing_template'
                        self.newSettings['template_name'] = 'New Template'
                        self.newSettings['template_subject'] = 'U2FtcGxlIFN1YmplY3Q=' 
                        self.newSettings['email_service'] = 'mailjet'
                        self.newSettings['content_type'] = 'text/html'
                        self.newSettings['has_links'] = 'yes'
                        self.newSettings['template_body'] = "VGhpcyBpcyBhIHBoaXNoaW5nIHRlbXBsYXRlLg=="
                        self.newSettings["shorten_links"] =  "yes"
                        self.newSettings["phishing_link"] = "random"
                else:
                        ##Look for template on DB
                        phishingTemplateQuery = {"setting_type":"phishing_template",
                                                "template_id": self.phishingTemplateID }
                        phishingTemplate = self.db.settings.find_one(phishingTemplateQuery)
                        self.newSettings['template_name'] = phishingTemplate['template_name']
                        self.newSettings['template_subject'] = phishingTemplate['template_subject'] 
                        self.newSettings['email_service'] = phishingTemplate['email_service']
                        self.newSettings['content_type'] = phishingTemplate['content_type']
                        self.newSettings['has_links'] = phishingTemplate['has_links']
                        self.newSettings['template_body'] = phishingTemplate['template_body']
                        self.newSettings["shorten_links"] =  phishingTemplate['shorten_links']
                        self.newSettings["phishing_link"] = phishingTemplate['phishing_link']
                        self.newSettings['setting_type'] = 'phishing_template_customized'
                        self.newSettings["parent_campaign"] = self.phishingCampaignID
                self.newSettings['creation_date'] = datetime.now()
                self.newSettings['template_id'] = str(uuid.uuid4())
                self.newSettings['editmode'] = 'yes'
                return self.newSettings
        
        def createNewTemplate(self):
                newSettings = self.createTemplateSettings()
                self.db.settings.insert(newSettings)
                return newSettings
                
        def loadSettings(self):
                return self.db.settings.find_one(self.objectQuery)
        
        def loadCampaignSettings(self):
                '''Phishing Settigns and customized templates are on different objects
                so two queries will happen and joined before returned to main program'''
                lastCampaignQuery = {"setting_type":"phishing_campaign",
                                   "last_used": "yes"}
        
                ## This query does not containg templates
                campaignSettings = self.db.settings.find_one(lastCampaignQuery)
                ## Search for attached template
                #print "#### LoadCampSttings"
                #print campaignSettings["campaign_id"]
                
                templatesQuery = {"setting_type":"phishing_template_customized",
                                   "parent_campaign": campaignSettings["campaign_id"]}
                
                customizedTemplates = self.db.settings.find(templatesQuery)
                
                phishing_templates = []
                
                for template in customizedTemplates:
                        ##- Look for the specific email service
                        emailService = self.db.settings.find_one({"item_id":template["email_service"]})
                        template["email_service"] = emailService
                        phishing_templates.append(template)
                        
                campaignSettings["phishing_templates"] = phishing_templates
                campaignSettings["newaddition"] = "yes"
        
                return campaignSettings
        
        def saveSettings( self, campaign_settings = None ):
                if self.phishingCampaignID  is None:
                        newSettings = self.createCampaignSettings()
                        self.phishingCampaignID = newSettings['campaign_id']
                        self.db.settings.insert( newSettings )
                        return newSettings
                else:
                        print "(DBG phishing.saveSettings) " + str(campaign_settings)
                        query = {'campaign_id': self.phishingCampaignID }
                        print "(DBG phishing.saveSettings) " + str(query)
                        results = self.db.settings.update_one( query, {'$set': campaign_settings }, upsert = True  )
                        
        
        def loadTargets(self, targetid):
                target_list = self.db.settings.find_one({'targetlist_id': targetid})
                return target_list['targets']
        
        def loadCustomizedLandingPages(self, campaign):
                lplist = self.db.settings.find_one({'setting_type': 'landingpage_template_customized', 'campaign_id':campaign})
                return lplist
        
        def getRandomURLPhishing(self):
                randomurl = requests.get("https://ch4lrnxatg.execute-api.us-east-1.amazonaws.com/ver1/Random_URL")
                randomurl = json.loads(randomurl.text)
                phishingURL = randomurl['resource'][0:-1]
                return phishingURL
                
                
        ##def getRandomURLPhishing(self):
        ##        with open("feeds/urls", "r") as feeds:
        ##                urlList = feeds.readlines()
        ##                phishingURL = urlList[ random.randint(1, len(urlList)) ]
        ##                return phishingURL
                        
        def runCampaign(self):
                '''1. Get settings of the campaign
                        1a. Create a Run objet as a settings to mantainf information about the run
                   2. Get the list of targets
                   3. For each target the entire list of templates is sent
                '''
                #- Run Settings Object
                runSettings = {}
                runSettings['run_id'] = str(uuid.uuid4())
                runSettings['campaign_id'] = self.phishingCampaignID
                runSettings['state'] = 'running'
                runSettings['setting_type'] = 'campaign_run' 
                runSettings['start_time'] = datetime.now()
                runSettings['finish_time'] = 0
                #- Insert Run object
                self.db.settings.insert( runSettings )
                #--------------------------------------
                settings = self.loadCampaignSettings()
                templates = settings['phishing_templates']
                targets = self.loadTargets(targetid = settings['target_email_list'])
                landingpage = self.loadCustomizedLandingPages(campaign = self.phishingCampaignID)
                logExtraMetadata = {"log_type":"campaign_run", "campaign_id": self.phishingCampaignID, "run_id": runSettings['run_id']}
                for target in targets:
                        #- Logging
                        ##logExtraMetadata = {'campaign_id': self.phishingCampaignID, 'feature': 'phishing', 'template_id': template['template_id']}
                        #logExtraMetadata = {"log_type":"campaign_run", "campaign_id": self.phishingCampaignID, "run_id": runSettings['run_id']}
                        log_message = pmsg.PMSG001( target = target['email'] )
                        print log_message
                        self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                        #- Logging ------------------------------------------------------------
                        for template in templates:
                                try:  
                                        self.sendEmail(target = target, template = template, landingpage = landingpage,  runID = runSettings['run_id'])
                                        
                                except Exception as e:
                                        ##logExtraMetadata = {'campaign_id': self.phishingCampaignID, 'feature': 'phishing', 'template_id': template['template_id']}
                                        #logExtraMetadata = {'campaign_id': self.phishingCampaignID, 'feature': 'phishing' , 'run_id': runSettings['run_id']}
                                        log_message = pmsg.PMSG004( error = e)
                                        print log_message
                                        self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                                        #- Logging ------------------------------------------------------------
                                        print e
                                time.sleep(int(settings['delay_between_emails']))
                #- Logging
                run = str(runSettings['run_id']) + "  @  " + str(self.phishingCampaignID)
                
                log_message = pmsg.PMSG010( runatcampaign = run )
                print log_message
                self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                #- Logging ------------
                #- Set stop to running settings
                results = self.db.settings.update_one( {"setting_type":"campaign_run","campaign_id": self.phishingCampaignID, "run_id": runSettings['run_id'] },
                                                        {"$set": {"state":'finished'} } )
                return 0
        
        def shorten_links(self, url = None, shortener = None ):
                if shortener == 'goolnk':
                        api_endpoint = 'https://goolnk.com/api/v1/shorten'
                        results = requests.post(api_endpoint,data = {'url': url})
                        short_url = json.loads(results.text)['result_url']
                        return short_url
                elif shortener =='tinyurl':
                        api_endpoint = 'http://tinyurl.com/api-create.php?url='
                        results = requests.get(api_endpoint + url)
                        short_url = results.text
                        return short_url
                elif shortener == 'qr':
                        pass
        
        
        def sendEmail( self, target = None, template = None, landingpage = None, runID = None ):
                logExtraMetadata={}
                logExtraMetadata = {"campaign_id": self.phishingCampaignID, "log_type":"campaign_run", "template_id": template['template_id'], 'run_id': runID}
                #self.mongoLogger.      info(log_message ,extra=logExtraMetadata)
                #efrom_user = "pooma"
                #efrom_domain = "fusiondementes.net" 
                ##efrom = efrom_user + '@' + efrom_domain
                ### {u'username': u'kridzani', u'password': u'shJ)1+9V4H7rXx', u'setting_type': u'phishing_email_service', u'service_name': u'A2 Hosting',
                ### u'smtp_server': u'mail.kridzania.club', u'item_id': u'0821cd23-d5a4-47e2-b984-a5fa5bdb48cc', u'email_account': u'pooma@kridzania.lcub'}
                efrom = template['email_service']['email_account']
                send_to = target['email']
                #Enclosing outer Message
                email = MIMEMultipart()
                subject_template = Template(base64.standard_b64decode( template['template_subject']) )
                email['Subject'] = str(subject_template)
                #print "    - Subject: " + email['Subject']
                email['From'] = efrom
                #email['Date'] = formatdate(localtime=True)
                email['To'] = send_to
                
                ## Set attachment
                #msg.attach(MIMEText(message))
                msg = MIMEBase('application','zip')
                # Set email message
                # Turn these into plain/html MIMEText objects
                phishingTemplate = Template(base64.standard_b64decode(template['template_body']))
                ## TIME TO CHOOSE THE PHISHING URL
                #- If Random: sistem will choose one from openphish feed
                #- If custom: sistem will get the preset Phishing URL
                #- From a template:  Phishers, will host the URL and add features as to capture user credentials.
                if landingpage['template_body'] == 'random':              
                        #Get a Ranom URL for testing Phishing tools
                        phishing_url = self.getRandomURLPhishing()
                else:
                        phishing_url = 'http://pooma.fusiondementes.net/landingpage?lpid=' + landingpage['landingpage_id']
                
                #- Logging
                #logExtraMetadata = {'campaign_id': self.phishingCampaignID, 'log_type':'campaign_run', 'template_id': template['template_id'],
                #                       'run_id': runID}
                log_message = pmsg.PMSG002( emfrom = efrom, emsendto = send_to, purl = phishing_url , subject = email['Subject'], template = template['template_id'] )
                print log_message
                self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                #- Logging ------------------------------------------------------------
                
                #Should we shorten the url?
                if template['shorten_links'] in ['goolnk','tinyurl']:
                        phishing_url = self.shorten_links(url = phishing_url, shortener = template['shorten_links'] )
                        #- Logging
                        #logExtraMetadata = {'campaign_id': self.phishingCampaignID, 'feature': 'phishing', 'template_id': template['template_id'],
                        #                'run_id': runID}
                        log_message = pmsg.PMSG003( shortener = template['shorten_links'], shortened_url = phishing_url )
                        print log_message
                        self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                        #- Logging ------------
                        
                template['url_as_qrcode'] = True       
                if template['url_as_qrcode'] == True:
                        #- Converto to QRCode this URL and return the link where the image is stored
                        headers = { "X-API-KEY": "F5AuigKjSfYRNMdxdTsS3axuRkJv8sS3b03P3uP1" }
                        apiEndpoint = "https://2077hazaie.execute-api.us-east-1.amazonaws.com/ver1/QRCodeGenerator/"
                        
                        encodingRequest = {"OriginalText": phishing_url }
                        #print encodingRequest
                        apiResponse = requests.post( apiEndpoint, headers = headers, data = json.dumps(encodingRequest))
                        #print apiResponse.text
                        ##
                        qrcode_image_url = json.loads(apiResponse.text)
                        qrcode_image_url = qrcode_image_url["ImageURL"]
                        ##- Logging
                        log_message = pmsg.PMSG006( qr_code_url = qrcode_image_url)
                        print log_message
                        self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                        ##- Logging
                else:
                        qrcode_image_url  = ""
                ### This are the valid tokes to be used inside the email body
                ## - url = is the Phishing URL . if not specified, it will be random.
                ## - tagert_name
                ## - target_email
                phishingTemplate.qrcode_image_url = qrcode_image_url
                phishingTemplate.url = phishing_url
                phishingTemplate.target_email = target['email']
                phishingBody = MIMEText(str(phishingTemplate), "html" )
                              
                ##part2 = MIMEText(html, "html")
                
                # Add HTML/plain-text parts to MIMEMultipart message
                # The email client will try to render the last part first
                ##msg.attach(part1)
                ##message.attach(part2)
                encoders.encode_base64(msg)
                #msg.add_header('Content-Disposition', 'attachment', filename=attachment.filename)
                email.attach(msg)
                email.attach(phishingBody)
                ##- Send log Before Sending
                #log_message = "Sending template: " + template['template_name']
                #print log_message
                #print "Using Email Service: " + template['email_service']['smtp_server'] + " - " + efrom
                #print ""
                #self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                #log_message = "Sending template: " + template['template_name']
                #self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                ##- 
                server = smtplib.SMTP_SSL(host=template['email_service']['smtp_server'], port=template['email_service']['smtp_port'])
                server.login(template['email_service']['username'],template['email_service']['password'])
                server.sendmail(efrom, send_to, email.as_string())
                server.close()
                ## -- Logging
                log_message = pmsg.PMSG005( email_service = template['email_service']['smtp_server'], port=template['email_service']['smtp_port'] )
                print log_message
                self.mongoLogger.info(log_message ,extra=logExtraMetadata)
                return 0   
        
        #####       End Of SendPhishing        
        
        
        


