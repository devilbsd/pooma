from Cheetah.Template import Template

def PMSG001(target = None):
	return "Starting Phishing Attack for TARGET: " + target

def PMSG002(emfrom = None, emsendto = None, template = None, subject = None, purl = None):
	return "Phishing Email has been created for TEMPLATE: " + template + " SUBJECT: " + subject + " FROM: " + emfrom + " TARGET: " + emsendto + " PHISHING URL: "  + purl

def PMSG003(shortener = None, shortened_url = None):
	return "Shortener service " + shortener + " was used. Shortened URL is " + shortened_url

def PMSG004( error = None):
	return "There was an ERROR when sending this email."

def PMSG005( email_service = None, port = None):
	return "Email was sent using EMAIL SERVICE: " + email_service + " using PORT " + str(port)

def PMSG006( qr_code_url = None):
	return "QR Code service was used. QR Code Image URL is : " + qr_code_url

def PMSG010(runatcampaign = None):
	return "Run " + runatcampaign + " has finished."