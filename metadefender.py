import config
import messages
import messages as msg
import mimetypes
import requests
import json
from pymongo import MongoClient
import gridfs
import logging
from log4mongo.handlers import MongoHandler
import threading
import time
import uuid
import base64

client = MongoClient(config.mongodburi)
db = client.pooma
repofs = gridfs.GridFS(db)

mongoLogger = logging.getLogger('PooMALog')
mongoHandler = MongoHandler(host=config.mongodburi,
                            database_name='pooma',
                            collection='logs')
mongoLogger.addHandler(mongoHandler)

def analyze(hashsum = None, analysisID= None):
	'''Endpoint for metadefender can be:
	hash = do lookups for MD5,nSHA1 & SHA256 reputation
	'''
	##  Search for the policy of the new analisis with the label
	##  "analysis_type" = "on-demand"
	headers = {'apikey': config.apikey}
	
	metadefender_results = {}
	metadefender_results['part_of_analysis'] = analysisID
	metadefender_results['feed_vendor'] = 'metadefender'
	metadefender_results['malwr_md5'] = hashsum
	metadefender_results['sample_md5'] = hashsum
	
	logExtraMetadata = {'sampleID': hashsum, 'feature': 'OPSW', 'analysisID': analysisID}
	## Check Reputation of the file
	url = "https://api.metadefender.com/v2/hash/"
	opsw_msg = "Asking Metadefender for hash: "+hashsum+" reputation."
	mongoLogger.info(opsw_msg ,extra=logExtraMetadata)
	
	response = requests.request("GET", url + hashsum, headers=headers)
	metadefender_results['metadefender'] =  json.loads(response.text)
	
	opsw_msg = "Malware reputation lookup from Metadefender core: " +"<p>"+ response.text +"</p>"
	mongoLogger.info(opsw_msg,extra=logExtraMetadata)
		
	try:
		scans = metadefender_results['metadefender']['scan_results']['scan_details']
		scans = removeDots(scans=scans)
		metadefender_results['metadefender']['scan_results']['scan_details'] = scans	
		
		print '=' * 80
		for i in scans:
			print i
		print '=' * 80
		db.analysis.insert_one(metadefender_results)
	except KeyError:
		print "File not in DB"
		opsw_msg = "Hash "+hashsum+" is NOT FOUND at OPSWAT Metadefender database"
		mongoLogger.info(opsw_msg ,extra=logExtraMetadata)
		db.analysis.insert_one(metadefender_results)
		# After reputation
		url = "https://api.metadefender.com/v2/file"
		fileMetadata = db.fs.files.find_one({"md5": hashsum})
		# Max number of tries for getting the results of a file scan
		maxTries = 4
		# Seconds to wait between polling
		pollingTime = 30
		# Upload fiel to Metadefender
		with repofs.get(fileMetadata['_id']) as filesample:
			headers['content-type'] = 'application/octet-stream'
			headers['filename']=filesample.filename
			
			opsw_msg = "Upload file to OPSWAT Metadefender fo Multiple AV analysis: " + filesample.filename
			mongoLogger.info(opsw_msg ,extra=logExtraMetadata)
			
			opsw_msg = "Request Headers: " + str(headers)
			mongoLogger.info(opsw_msg ,extra=logExtraMetadata)
			
			results = requests.post( url, headers = headers, data = filesample)
			
			opsw_msg = "Request Response: " + results.text
			mongoLogger.info(opsw_msg ,extra=logExtraMetadata)
			
			# Update metadata before starting comparison, this is the first update from
			analysisMetadata = db.analysis.find_one({"sample_md5": hashsum, "part_of_analysis": analysisID, "feed_vendor":"metadefender"})
			documentID =  analysisMetadata['_id']
			db.analysis.update({'_id': documentID}, {'$set': {'metadefender': json.loads(results.text) }})
			#print results.text
			res = json.loads(results.text)
			url = "https://api.metadefender.com/v2/file/" + res["data_id"]
			
			while maxTries > 0:
				print "### Try :" + str(maxTries) 
				print url
				response = requests.get(url, headers=headers)
				db.analysis.update({'_id': documentID}, {'$set': {'metadefender': json.loads(response.text) }})
				time.sleep(pollingTime)
				maxTries = maxTries - 1

def removeDots(scans=None):
	try:
		drweb = scans["Dr.Web Gateway"]
		scans.pop("Dr.Web Gateway")
		scans["Dr_Web Gateway"] = drweb
	except KeyError:
		pass
	try:
		virit = scans["Vir.IT eXplorer"]
		scans.pop("Vir.IT eXplorer")
		scans["Vir_IT eXplorer"] = virit
	except KeyError:
		pass
	try:
		virit = scans["Vir.IT ML"]
		scans.pop("Vir.IT ML")
		scans["Vir_IT ML"] = virit
	except KeyError:
		pass
	return scans




