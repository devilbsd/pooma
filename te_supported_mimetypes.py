MIMETYPELIST = []
MIMETYPELIST = ['application/pdf',
            # EXECUTABLES
            'application/x-dosexec',
            'application/x-executable',
            # ARCHIVES
            'application/x-tar',
            'application/x-gtar',
            'application/x-bzip',
            'application/x-bzip2',
            'application/zip',
            'application/gzip',
        	'application/x-rar',
        	'application/x-7z-compressed',
        	'application/vnd.ms-cab-compressed', #Cabinet
        	# DOCUMENTS
        	'application/CDFV2-corrupt',
            'application/vnd.ms-word.document.macroenabled.12', #docm
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', #docx
            'application/vnd.ms-word.template.macroenabled.12', #dotm
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template', #dotx
        	'application/vnd.openxmlformats-officedocument.presentationml.presentation', #pptx
        	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', #xlsx
        	'application/java-archive', #jar
        	'text/plain',
        	'application/x-shockwave-flash',
            'text/rtf',
            'text/html',
	    'application/xml',	

            # IMAGES
            'image/gif',
            'image/png',
            'image/jpeg',
            'image/tiff']
