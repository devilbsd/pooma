#!/usr/bin/env bash
#- Bash Agent for PooMA v.0.1 superbeta
#- Javier Padilla
#- jpadilla@checkpoint.com
#-----------------------------------------------------------------------------+
CURL='curl'
HOSTNAME=\$(hostname)
DATE=\$(date +%s)
UPLOADURL="http://172.20.22.66/upload"
REPODIR=("put here dir")
ERRORFILE="/var/log/file_monitor_agent.log"

function send_sample {
    echo ""
    echo "[ Sending sample to Server ]"
    echo "File: \$filename"
    RESPONSE=\$(\$CURL -s -F origin=\$HOSTNAME -F customer_file=@\$filename \$UPLOADURL)
    ##echo \$RESPONSE
}

echo ""
echo "[ \$(date) -- Starting Malware Monitoring ---- ]"
echo "- Reading files from \$REPODIR"

for TARGETDIR in \${REPODIR[@]}; do
    
for filename in "\$TARGETDIR"*; do
    send_sample
done

done


#-
# Enf of agent script
#-----------------------------------------------------------------------------+
