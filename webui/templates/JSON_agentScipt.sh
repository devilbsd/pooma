#!/usr/bin/env bash
#- Bash Agent for PooMA v.0.1 superbeta
#- Javier Padilla
#- jpadilla@checkpoint.com
#-----------------------------------------------------------------------------+
CURL='curl'
HOSTNAME=\$(hostname)
DATE=\$(date +%s)
SERVERHOST="http://pooma.fusiondementes.net"
REPODIR=(
#for $dir in $target_dir
#set $dir = $dir.rstrip()
"$dir"
#end for
)
ERRORFILE="/var/log/file_monitor_agent.log"
JSON="./JSON.sh"

function send_sample {
    echo ""
    echo "[ Sending sample to Server ]"
    echo "File: \$filename"
    echo "md5: \$SAMPLE_MD5"	
    SAMPLEDATA='{"origin":"'\$HOSTNAME'","md5":"'\$SAMPLE_MD5'","filename":"'\$filename'","fileb64":"'\$BASE64FILE'","filetype":"'\$FILETYPE'"}'
    RESPONSE=\$(\$CURL -s -H "Accept: application/json" -H "Content-Type: application\/json" -X POST -d \$SAMPLEDATA \$SERVERHOST/sample/)
    echo \$RESPONSE
    STATUS=\$(echo \$RESPONSE | \$JSON -b | awk '/\["errorCode"\].+"(.+)"/ {print $2}' | tr -d '"')
    MSG=\$(echo \$RESPONSE | \$JSON -b | awk '/\["errorMsg"\].+"(.+)"/ {print $2}' | tr -d  '"')
    case STATUS in
        200)
            echo "[ \$STATUS ] \$MSG"
            exit 0
            ;;
        201)
            echo "[ \$STATUS ] \$MSG"
            exit 0
            ;;
        *)
            echo "Something is Wrong! Not able to submit sample."
            echo \$RESPONSE >> \$ERRORFILE
            ;;
    esac
}

echo ""
echo "[ \$(date) -- Starting Malware Monitoring ---- ]"
echo "- Reading files from \$REPODIR"

for TARGETDIR in \${REPODIR[@]}; do
    
for filename in "\$TARGETDIR"*; do
    SAMPLE_MD5=\$(md5 -q \$filename)
    BASE64FILE=\$(base64 \$filename | tr -d '\n')
    FILETYPE=\$(file \$filename | base64 | tr -d '\n')
    send_sample
done

done


#-
# Enf of agent script
#-----------------------------------------------------------------------------+
