MIMETYPELIST = []
MIMETYPELIST = [
	# ACROBAT PDF
	'application/pdf',
	'application/zip',
	# MS WORD
	'application/CDFV2-corrupt',
            'application/vnd.ms-word.document.macroenabled.12', #docm
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document', #docx
            'application/vnd.ms-word.template.macroenabled.12', #dotm
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template' #dotx
	'application/CDFV2-corrupt',
	'application/vnd.openxmlformats-officedocument.presentationml.presentation', #pptx
	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	   'application/xml',
    'text/rtf',
    'image/gif',
    'image/png',
    'image/jpeg',
    'image/tiff'] #xlsx