#This is a tmp DB of commands that will be inserted at the beginning
#on DB
## THREAT PREVENTION
### TED

tecli_ss = {}
tecli_ss['name'] = 'TE: tecli show statistics'
tecli_ss['cmd'] = 'tecli s s'

tecli_sda = {}
tecli_sda['name'] = 'TE: tecli show download all'
tecli_sda['cmd'] = 'tecli s d a'

tecli_sdagrep = {}
tecli_sdagrep['name'] = 'TE: Show status of downloads'
tecli_sdagrep['cmd'] = "tecli s d a | grep -A1 'Status'"

tecli_venvs = {}
tecli_venvs['name'] = 'TE: Show available VMs'
tecli_venvs['cmd'] = '''tecli sh do dump | jq -r " .images[]  | [.image_name, .uid,.revisions[].revision_id ] | @csv "'''


tecli_sevs = {}
tecli_sevs['name'] = 'TE: tecli shows emulation synopsis'
tecli_sevs['cmd'] = 'tecli s e v s'

tecli_see = {}
tecli_see['name'] = 'TE: tecli shows emulator emulations'
tecli_see['cmd'] = 'tecli s emu emu'

tetrp = {}
tetrp['name'] = 'TE: Show troughput'
tetrp['cmd'] = 'tecli show throughput minute && tecli show throughput hour && tecli show throughput day && tecli show throughput month'

teadvattr = {}
teadvattr['name'] = 'TE: Show advanced attributes'
teadvattr['cmd'] = 'tecli advanced attributes show'

tecli_eb = {}
tecli_eb['name'] = 'TE: Shows Early Verdict status'
tecli_eb['cmd'] = 'tecli advanced part_response local stat	&& tecli advanced part_response remote stat && tecli advanced part_response cloud stat	'

tedelg = {}
tedelg['name'] = 'TE: $FWDIR/log/ted.elg'
tedelg['cmd'] = 'cat $FWDIR/log/ted.elg'

tedebugon = {}
tedebugon['name'] = 'TE: Enable TED debug'
tedebugon['cmd'] = 'tecli debug on'

tedebugoff = {}
tedebugoff['name'] = 'TE: Disable TED debug'
tedebugoff['cmd'] = 'tecli debug off'

tedebugclean = {}
tedebugclean['name'] = 'TE: Clean all TED debug files'
tedebugclean['cmd'] = 'tecli debug clean'

tevncdbgon = {}
tevncdbgon['name'] = 'TE: Enable VNC Debug'
tevncdbgon['cmd'] = ' tecli debug emulation enable'

tevncdbgoff = {}
tevncdbgoff['name'] = 'TE: Disable VNC Debug'
tevncdbgoff['cmd'] = ' tecli debug emulation disable'

tecldquota = {}
tecldquota['name'] = 'TE: Show cloud quota'
tecldquota['cmd'] = 'tecli show cloud quota'

tecldqueue = {}
tecldqueue['name'] = 'TE: Show cloud queue'
tecldqueue['cmd'] = 'tecli show cloud queue'

tecldid = {}
tecldid['name'] = 'TE: Show GW ID for Cloud'
tecldid['cmd'] = 'tecli show cloud identity'

teclicachettl = {}
teclicachettl['name'] = 'TE: Show local cache TTL'
teclicachettl['cmd'] = 'tecli cache ttl display'

teclicachedump = {}
teclicachedump['name'] = 'TE: Show local cache'
teclicachedump['cmd'] = 'tecli cache dump all'

teclicacheclean = {}
teclicacheclean['name'] = 'TE: Clean local cache'
teclicacheclean['cmd'] = 'tecli cache clean'

tpapi = {}
tpapi['name'] = 'TE: $FWDIR/TPAPI.ini'
tpapi['cmd'] = 'cat $FWDIR/TPAPI.ini'

tefileclass = {}
tefileclass['name'] = 'TE: $FWDIR/te/update/file_classification.conf'
tefileclass['cmd'] = 'cat $FWDIR/te/update/file_classification.conf'	

tefiletypes = {}
tefiletypes['name'] = 'TE: $FWDIR/te/update/te_file_types.xml'
tefiletypes['cmd'] = 'cat $FWDIR/te/update/te_file_types.xml'

teconf = {}
teconf['name'] = 'TE: $FWDIR/conf/te.conf'
teconf['cmd'] = 'cat $FWDIR/conf/te.conf'

tecpstmlwr = {}
tecpstmlwr['name'] = 'TE: cpstat te file_type_stat_malware_detected'
tecpstmlwr['cmd'] = 'cpstat threat-emulation -f file_type_stat_malware_detected'



# TEX Debug

texdbgon = {}
texdbgon['name'] = 'TEX: Start Debug '
texdbgon['cmd'] = 'scrub debug on && scrub debug set all all && for PROC in $(pgrep cp_file_convert) ; do fw debug $PROC on TDERROR_ALL_ALL=5 ; done && scrub debug stat'

texdbgstatus = {}
texdbgstatus['name'] = 'TEX: Debug status'
texdbgstatus['cmd'] = 'scrub debug stat'

scrubelg = {}
scrubelg['name'] = 'TEX: $FWDIR/log/scrubd.elg'
scrubelg['cmd'] = 'cat $FWDIR/log/scrubd.elg'

convertedelg = {}
convertedelg['name'] = 'TEX: $FWDIR/log/scrub_cp_file_converted.elg'
convertedelg['cmd'] = 'cat /var/log/jail$FWDIR/log/scrub_cp_file_convertd.elg'

texdbgoff = {}
texdbgoff['name'] = 'TEX: Stop Debug '
texdbgoff['cmd'] = 'for PROC in $(pgrep cp_file_convert) ; do fw debug $PROC on TDERROR_ALL_ALL=5 ; done && scrub debug off && scrub debug reset && scrub debug stat'

texdbgrotate = {}
texdbgrotate['name'] = 'TEX: Debug log rotate'
texdbgrotate['cmd'] = 'scrub debug rotate'
## AV

mlwrmatch = {}
mlwrmatch['name'] = 'AV: Matching malware table '
mlwrmatch['cmd'] = ' fw tab -t mal_conns '

malconfig = {}
malconfig['name'] = 'AV: $FWDIR/conf/malware_config'
malconfig['cmd'] = 'cat $FWDIR/conf/malware_config'
malconfigrules = {}
malconfigrules['uuid-001'] = {'name':'DNS Poliy on background mode',
							  'regex':'xxxxx',
							  'resources':['SK001','SK002']}
malconfig['rules'] = malconfigrules

## ICAP
icapVirusScanConf = {}
icapVirusScanConf['name'] = 'ICAP: Show virus_scan.conf'
icapVirusScanConf['cmd'] = 'cat $FWDIR/c-icap/etc/virus_scan.conf'

icapServerLog = {}
icapServerLog['name'] = 'ICAP: Show server.log'
icapServerLog['cmd'] = 'cat $FWDIR/log/c-icap/server.log'

icapServerConf = {}
icapServerConf['name'] = 'ICAP: Show C-ICAP config file'
icapServerConf['cmd'] = 'cat  $FWDIR/c-icap/etc/c-icap.conf'


# YARA
yara_show = {}
yara_show['name'] = 'YARA: Show Status'
yara_show['cmd'] = 'tecli advanced yara show'

yara_enable = {}
yara_enable['name'] = 'YARA: Enable YARA Engine'
yara_enable['cmd'] = 'tecli advanced yara set 1'

yara_disable = {}
yara_disable['name'] = 'YARA: Disable YARA Engine'
yara_disable['cmd'] = 'tecli advanced yara set 0'



TE_CLI = [tecli_ss, tecli_sevs, tecli_see,
		  tecli_sda, tecli_sdagrep, tecli_venvs,
		  tetrp, tecli_eb, tedelg,
		  tedebugon, tedebugoff, tedebugclean,
		  tevncdbgon, tevncdbgoff, tecldquota,
		  tecldqueue, tecldid, teclicachettl,
		  teclicachedump, teclicacheclean, tefileclass,
		  tefiletypes, tpapi,teconf,tecpstmlwr,
		  texdbgon, texdbgstatus, scrubelg,
		  convertedelg, texdbgoff, texdbgrotate,
		  malconfig, mlwrmatch, icapVirusScanConf,
		  icapServerLog, icapServerConf,
		  yara_enable, yara_show, yara_disable]


#print malconfig
## NGFW

version = {}
version['name'] = 'FW: Gateway version'
version['cmd'] = 'hostname && fw ver -k && uname -a'

instjha = {}
instjha['name'] = 'FW: Show installed JHA'
instjha['cmd'] = 'installed_jumbo_take'

fwtabconnections = {}
fwtabconnections['name'] = 'FW: Connections table'
fwtabconnections['cmd'] = 'fw tab -t connections -s'

cpwd = {}
cpwd['name'] = 'FW: CP WatchDog Process List'
cpwd['cmd'] = 'cpwd_admin list'

cpstat = {}
cpstat['name'] = 'CP: cpstat'
cpstat['cmd'] = 'cpstat'

cplicprint = {}
cplicprint['name'] = 'CP: cplic print'
cplicprint['cmd'] = 'cplic print'

## GAIA
## OS

df = {}
df['name'] = 'OS: Free Disk Space'
df['cmd'] = 'df -h'

psfea = {}
psfea['name'] = 'OS: List of all proceses'
psfea['cmd'] = 'ps -fea'

netstatna = {}
netstatna['name'] = 'OS: Active network ports'
netstatna['cmd'] = 'netstat -na'

netstatnr = {}
netstatnr['name'] = 'OS: Show static routes'
netstatnr['cmd'] = 'netstat -nr'


top = {}
top['name'] = 'OS: Top'
top['cmd'] = 'top -b -n 1'

lslog = {}
lslog['name'] = 'OS: List at $FWDIR/log/'
lslog['cmd'] = 'ls -lh $FWDIR/log/'

ifconfiga = {}
ifconfiga['name'] = 'OS: ifconfig -a'
ifconfiga['cmd'] = 'ifconfig -a'

shintall = {}
shintall['name'] = 'CLISH: show interfaces all'
shintall['cmd'] = '''clish -c "show interfaces all"'''

logmessages = {}
logmessages['name'] = 'OS: Show /var/log/messages'
logmessages['cmd'] = 'cat /var/log/messages'

reboot = {}
reboot['name'] = 'OS: Reboot  WARNING!'
reboot['cmd'] = 'reboot'

shutdown = {}
shutdown['name'] = 'OS: Shutdown  WARNING!'
shutdown['cmd'] = 'shutdown -h now'


GAIA = [df, psfea, netstatna, netstatnr, top, lslog, logmessages, ifconfiga, shintall, reboot, shutdown]


NGFW = [version, instjha, fwtabconnections, cpwd, cplicprint, cpstat]
