from Cheetah.Template import Template


TEX_SUPPORTED = '''
> MIME-type ( %s ) IS supported by Check Point SandBlast Threat Extraction.\n'''

TEX_ENABLED='''
> Per settings at Pooma this file will be sent Cloud TEX.\n'''

TEX_NOT_ENABLED='''
> MIME-type ( %s ) IS NOT supported by Check Point SandBlast Threat Extraction.\n'''

TEX_FINALIZED='''
> TEX process has finalized, fetched documents are free of any threat, enjoy!'''

AVTETEX_DOCUMENTS_NOT_ENABLED='''> Query for this file is empty\n> Analysis of Documents NOT Enabled\n\n'''

ASK_STATUS='File has been already uploaded. Request IS NOT not uploading the file. Asking for status ...'

NO_COOKIE='There is NO cookie! This is a new analysis.'

def THEREIS_COOKIE(cookie=None):
	return "This is an on-going analysis using cookie " + cookie

def RECEIVED_FILE(filename=None, origin=None, sampleID=None):
	return "Received file: "+filename+" from sorce: "+origin+". Sample ID is: " + sampleID

def MIMETYPE_NOT_SUPPORTED(mimetype=None):
	return '> Content Type-MIME: ' + mimetype + ' is NOT SUPPORTED for SandBlast Analysis\n\n'

def TED_NOT_SUPPORTED(mimetype=None):
	return 'Content Type-MIME: ' + mimetype + ' is NOT SUPPORTED for SandBlast Analysis.Malware analysis for this file is CANCELED.'

def TED_SUPPORTED(mimetype=None):
	return 'Content Type-MIME: ' + mimetype + ' is SUPPORTED for SandBlast Analysis, I will start TE thread'

def START_THREAD(tries=None):
	return 'Starting new thread for malware analysis. I will TIMEOUT after ' + str(tries) + 'tries.'

def START_TEX_THREAD(tries=None):
	return 'Starting new thread for malware EXTRACTION. I will TIMEOUT after ' + str(tries) + 'tries.'

def TEX_REQUEST(request=None):
	return 'PooMA will query TP API with the following request: \n' + str(request)

def TEX_REQUEST_STATUS(texStatus=None, response=None):
	REQ_STATUS = Template(file="log_templates/DBG_TEX_REQUEST_STATUS")
	REQ_STATUS.texStatus = texStatus
	REQ_STATUS.response = response
	return str(REQ_STATUS)

def TRY_STATUS(tries=None, status=None):
    return '[Try #'+ str(tries) +' ] File Status is : ' + status

def UPLOAD_ERROR(tries=None):
    return '[Try #'+ str(tries) +' ] Something weird happens, retrying in 3 seconds...'

TRY_EXHASUTED = 'Number of tries is exhausted, aborting analysis'
START_1ST_PHASE = 'Starting 1st Phase: AntiVirus Reputation.'
UPLOAD_SUCCESFULLY = 'File was SUCCESSFULLY uploaded to SandBlast, be patient the analysis is running'
FETCH_FORENSICS_REPORTS = 'Fetching forensics reports from XXX Virtual Environments'

def FILE_PENDING(tries=None, waitingTime=None):
	return '[ Try #' + str(tries) +' ] File is still being analyzed by SandBlast, waiting ' + str(waitingTime) + ' seconds.' 

def TE_FOUND(verdict=None, tries=None):
	return 'File found by Threat Emulation SANDBOX  with verdict: ' + verdict + 'on try #' + str(tries)

AV_MALICIOUS = 'File is FOUND as MALICIOUS by SandBlast reputation AV database.'


def TEX_NOT_SUPPORTED(mimetype=None):
	return 'Content Type-MIME: ' + mimetype + ' is NOT SUPPORTED for SandBlast Threat Extraction. Sanitizing process for this file is CANCELED.'

def TEX_SUPPORTED(mimetype=None):
	return 'Content Type-MIME: ' + mimetype + ' is SUPPORTED for SandBlast Threat Extraction, I will start SCRUBBING thread'

def COOKIE_MSG(cookies=None):
	if cookies == None:
		return '> SandBlast API did NOT return any cookie. This is a response from a local appliance'
	else:
		return '> SandBlast API returns cookie: ' + cookies

def FEED_SETTINGS(feedSettings=None):
	message = '''> Feed Settings: ''' + str(feedSettings) + '\n\n'
	return message

def FAILOVER_SETTINGS(feedSettings=None):
	message = '''> By Policy file will be analyzed first on location: %s, if the primary service cannot be contactetd it will fail over: %s''' % ( feedSettings['documents_primary'], feedSettings['documents_backup'] )
	return message

def FAILOVER_ACTION(backupService=None):
	message = '''> Connection to primary service failed, Failing-Over Backup: %s''' % backupService
	return message

def SBTOKEN(token=None):
	message = '''> The following KEY is going to be used: %s''' % token
	return message

def QUERY_RESULTS(avStatus=None, teStatus=None, texStatus=None, cookies=None):
	message = '''> Query API Response has the following results: avStatus = %s, teStatus = %s, texStatus = %s, cookie = %s ''' % (avStatus, teStatus, texStatus, cookies)
	return message

def UPLOAD_RESULTS(avStatus=None, teStatus=None, texStatus=None, cookies=None):
	message = '''> Upload API Response has the following results: avStatus = %s, teStatus = %s, texStatus = %s, cookie = %s ''' % (avStatus, teStatus, texStatus, cookies)
	return message

UPLOAD_FILE = 'Files is 1004 = NOT FOUND for at least 1 engine. UPLOADING file to SandBlast'


NEW_REQUEST = '> Content Type-MIME: %s is SUPPORTED for SandBlast Analysis\n> A new unique request was created.'

DOCUMENTS_NOT_ENABLED = '''> Policy for documents analysis is set to NOT_ENABLED, No query will be created.'''

DOCUMENTS_ENABLED = '''> Policy for documents analysis is set to ENABLED. A Query will be created.'''
