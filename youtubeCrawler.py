from __future__ import unicode_literals
import requests
from pymongo import MongoClient
import youtube_dl
import config
import time
from mutagen.id3 import ID3, APIC, error

client = MongoClient(config.mongodburi)
db = client.pooma

def get_url_from_queue():
    url = db.youtubedwld.find_one();
    return url

def del_url_from_queue(url=None):
    result = db.youtubedwld.delete_one({'url':url})
    print ">> URL Removed from Queue:"
    print ">> " + url
    return 0
    
while True:
    try:
        yt_url = get_url_from_queue()["url"]
    
        dwld_options = { 'format': 'bestaudio/best',
                        'archive_file':'youtube.history',
                        'writethumbnail':True,
                        'postprocessors':[{
                            'key':'FFmpegExtractAudio',
                            'preferredcodec' : 'mp3',
                            'preferredquality':'256'
                        }]}

        with youtube_dl.YoutubeDL(dwld_options) as ydl:
            print ">> Downloading : " + yt_url
            ydl.download([yt_url])
            metadata = ydl.extract_info(yt_url, download=False)
            #print metadata
            #print dir(ydl)
            #print metadata['title']
            #print metadata['id']

        
        del_url_from_queue(yt_url)    

    except TypeError:
        print ">> No URL in queue"

    time.sleep(20)
